# Minerva Special Samples

## How to produce them

The first thing to do is get access to the `minervapro` account on the Fermilab machines.  Assuming you already have access to Fermilab, post on the computing slack channel and someone can help you get access.  Then do a `kinit` and log in:

```
ssh -K minervapro@mnv01.fnal.gov
```

### Setting up the environment

*Note*: FNAL is now moving away from `jobsub_client`, onto a new system called `jobsub_lite`. There are numerous changes to the workflow below, so everything below the hrule is to be considered **obsolete**.

To setup jobsub_lite as minervapro, source the script `~/jobsublite_minset_v21r1p1_cvmfs.sh`. This will setup the binary and Production token for you; verify this is correct by running
`which jobsub_submit` (expected output: `/opt/jobsub_lite/bin/jobsub_submit`).

You should have a valid token: if you want to check explicitly you can hit `jobsub_q --user minervapro` and then `htdecodetoken -a /tmp/bt_token_minerva_production_43567`. You should see something like this:

```
{
  "typ": "JWT",
  "kid": <long string>,
  "alg": "RS256"
}
{
  "wlcg.ver": "1.0",
  "aud": "https://wlcg.cern.ch/jwt/v1/any",
  "sub": "minervapro@fnal.gov",
  <etc>
}
```

You should now be in the production folder:

```
/minerva/app/users/minervapro/cmtuser/Minerva_v21r1p1_CVMFS/Tools/ProductionScripts
```

Note that the production folder has had some of its job manager classes, such as `ProductionScripts/py_classes/CondorJobBuilder.py` and `ProductionScripts/py_classes/MinervaProductionTarball.py`, modified to remove the now-obsolete `--prefix` option, compared with v2*r1p1 CVMFS. So if you want to set up a new area with tag `tag`, you should do the following:

```
cp -r /minerva/app/users/minervapro/cmtuser/Minerva_v21r1p1_jobsublite/ /minerva/app/users/minervapro/cmtuser/Minerva_v21r1p1_<tag>
source ~/jobsublite_minset_v21r1p1_cvmfs.sh
cd /minerva/app/users/minervapro/cmtuser/Minerva_v21r1p1_<tag> && cp ~/jobsublite_minset_v21r1p1_cvmfs.sh ./minset_v21r1p1_<tag>.sh
sed -i $'s/tag=\"v21r1p1_CVMFS\"/tag=\"v21r1p1_<tag>\"/g' ./minset_v21r1p1_<tag>.sh
for FNAME in cleanup.sh cleanup.csh setup.sh setup.csh ; do sed -i $'s/Minerva_v21r1p1_jobsublite/Minerva_v21r1p1_<tag>/g' /minerva/app/users/minervapro/cmtuser/Minerva_v21r1p1_<tag>/Tools/ProductionScripts/cmt/${FNAME}; done
```

and thenceforth instead of sourcing the main `~/jobsublite_minset_v21r1p1_cvmfs.sh` you should first cd into your special tagged area and next source the `minset_v21r1p1_<tag>.sh` you made.

---

*Obsolete section, for future archaeology*:

The easiest thing to do is run the function `setup_abbey`.  Alternatively you can do the following:

```
source ~/minset_v21r1p1_cvmfs.sh; source ~/setup_minervapro_auth.sh
setup jobsub_client
```

### Running Genie stage

There are three situations you might find yourself in here:

1. You don't need to set `genie_var` because you are making a sample with specific run number identification (e.g. Oscar's ECal samples)
2. You are running a custom version of Genie.  In this case you need to install it and set up paths to point to it (follow the second example below, substituting your version for the HNL one).  You also need to make sure to set `genie_var`.
3. You are running a standard version of genie but have changed something else e.g. flux, geometry.  In this case follow the first set of instructions below and make sure to set `genie_var`


This is done with the file `submit_mc/submit_NX_Genie_Missing_New.py`.  To not mess things up for other users, make your own version of the file `submit_NX_Genie_Missing_New_<user>.py`.  You will now need to edit this file, and it is a good idead to turn on the debugging!  The two things that you need to change (at least!) are the `output location` and `genie_var` which all appear in the following function call:

```
submitJobs(runbase,<output location>,firstrun,lastrun,firstsubrun,lastsubrun,myPlaylist,isFrozen=False,genie_var=None)
```

Set the `<output location>` (real variable name??) to some string you will recognise later, and `genie_var` needs setting so that the file names will not be the same as the already produced production (or files may get overwritten later when we write to tape).  This should be a string to tag the particular special production.


Updated if you only want to change file names!  If this is the case, you need to edit `Tools/ProductionScripts/mc_scripts/ProcessMC.py` to add the new sample name inside the function `getKnownGENIEVars`.  Be careful when editing this script, everyone is using it!  The new name *must* contain the string `Signal` e.g. `MySpecialSignal`.  Then you just set `genie_var` in the previous script to be the same name and you are good to go.



In case you need to change your Genie configuration further here are John's instructions:


```
# from a clean session as minervapro
source /cvmfs/minerva.opensciencegrid.org/minerva/software_releases/v21r1p1/setup.sh
setenvMinerva <some_area>
cp -r /minerva/app/users/minervapro/cmtuser/Minerva_v21r1p1_HNL/* .
# go into minset_v21r1p1_HNL.sh and change all instances of "HNL" to your area tag.
# also go into Tools/ProductionScripts/cmt/setup.sh and change all "HNL" to your area tag
source ./minset_v21r1p1_HNL.sh
cd GENIE/cmt
source setup.sh # this will complain about not being able to figure out some cmt stuff, it's safe to ignore
# edit cmt/setup.sh: change /minerva/app/users/kplows/cmtuser/Minerva_v21r1p1 to your working area
printenv GENIE # this should point to your local install directory now
cd ../x86_64-slc6-gcc49-opt/GENIE/config
# edit UserPhysicsOptions.xml to contain <param_set name="your_tag"> instead of <param_set name="Default">
```


**EDIT**: Ryan tested this stack and found that the above is not enough. Turns out GENIE's AlgConfigPool is brilliantly evil, and asks for the environment variable `GUSERPHYSOPT` to be set to `your_tag`.

Normally setting `--genie_var <something>` in ProcessMC.py would be enough, but `py_classes/MCGenieStage.py` (which actually handles the GENIE wrapper creation) sneakily refuses to set `GUSERPHYSOPT` if the string `Signal` is contained in `genie_var`. So you need to modify `py_classes/MCGenieStage.py` as follows:

```
# in prepareExecutable(self):
# if self.opts["genie_var"] != "dummy" and self.opts["genie_var"].find("Signal")==-1:#passed a genie_var, make sure it isn't sig only
  if self.opts["genie_var"] != "dummy": #passed a genie_var
      reducedOpts = self.opts["genie_var"][0:self.opts["genie_var"].find("_Signal")]
      # wrapfile.write("export GUSERPHYSOPT=%s\n" % self.opts["genie_var"])
      wrapfile.write("export GUSERPHYSOPT=%s\n" % reducedOpts)
```

Make sure that, if you intend to name your special sample something like `flavor_swap_Signal`, then your tag inside UserPhysicsOpts.xml should be `<param_set name="flavor_swap">`.


Now you can run genie stage, go to `Tools/ProductionScripts/submit_mc` and do:

```
python submit_NX_Genie_Missing_New_<user>.py
```

## Making GENIE metadata

In order to save your samples to tape, it is necessary to make some metadata. This section assumes you have some sample of pre-made GENIE files, and that they adhere to the following naming convention:

```
# with $GENIE_VAR being whatever your sample name is called
grid/${GENIE_VAR}/minerva/genie/v21r1p1/${RUNFOLDERS}/SIM_minerva_${RUNNO}_${SUBNO}_GENIE_v21r1p1-${GENIE_VAR}_ghep.root
```

These GENIE files should live in a standalone directory. All of this directory's contents will be declared to tape, so only keep what you want declared in there. In this section we'll call it `SAMTEST`.

Copy the `genie` directory structure (without the files) to a new directory for the metadata, `genie_meta`. Let's say you have 2 runs, 111000 and 111500, and you've simulated subrun 1 from each. Your `SAMTEST` should look like this:

```
tree SAMTEST
SAMTEST/
-- grid
   -- $GENIE_VAR
      -- minerva
         -- v21r1p1
	    -- genie
	    |  -- 00
	    |     -- 11
	    |        -- 10
	    |        |  -- 00
	    |        |     -- SIM_$GENIE_VAR_00111000_0001_GENIE_v21r1p1-$GENIE_VAR_ghep.root
	    |        -- 15
            |           -- 00
	    |              -- SIM_$GENIE_VAR_00111500_0001_GENIE_v21r1p1-$GENIE_VAR_ghep.root
	    -- genie_meta
	       -- 00
	          -- 11
		     -- 10
		     |  -- 00
		     |
		     -- 15
		        -- 00
		     
```

Go into `<your_minervapro_cmtuser_area>/Tools/SystemTests/` and make a directory called `testSAM/`, and `cd` into there.

Copy the meta_scripts into this directory. You can either grab them directly from `/minerva/app/users/minervapro/cmtuser/Minerva_v21r1p1_HNL/Tools/SystemTests/testSAM/`, or from `meta_scripts` in this repo.

Edit `GENIE_VAR` in `doMakeMCMeta_TEST.sh` to read what your special sample is named. The metadata that we're interested in is `OUTPUTFILE, METADIR, GENIE_VAR`. You may also want to modify `EXECUTABLE` to the path of the original executable that generated your GENIE files (I've put a generic string in but you may want to remember what you called. It doesn't seem to impact the actual metadata). This will populate the `genie_meta` directories as such:

```
tree SAMTEST
SAMTEST/
<... stuff>

            -- genie_meta
	       -- 00
	          -- 11
		     -- 10
		     |  -- 00
		     |     -- SIM_minerva_00111000_0001_GENIE_v21r1p1-$GENIE_VAR_ghep.root.json
		     |        SIM_minerva_00111000_0001_GENIE_v21r1p1-$GENIE_VAR_ghep.root.metadata.py
<... etc>	      
```

You can then validate that this metadata is valid, by running

```
python declare2FTS_TESTJOHN.py test $(readlink -f SAMTEST/)
```

(WIP: `declare2FTS_TESTJOHN.py` only accepts `test` as arg-1. It should also accept `store`, in which case it would actually declare the contents of `genie_meta` to tape. This functionality is disabled until someone has tested this pipeline successfully.)
 
## How to run cal stage (from pre-made GENIE files)

*Note*: This section is meant for cases where you've made your own GENIE files **without** running the MINERvA GENIE stage. No metadata whatsoever are created with this method, so declaring this to tape is risky at best and **not** recommended. Therefore, **apply this section for small samples!!**

Cal stage expects to see nicely named GENIE samples in the form of v2 `*.ghep.root` files. The naming convention[^1] is as follows:

```
grid/central_value/minerva/genie/v21r1p1/${RUNFOLDERS}/SIM_minerva_${RUNNO}_${SUBNO}_GENIE_v21r1p1_ghep.root
```

[^1]: Beware if you're using custom GENIE tags as described in the preceding section. You probably need to replace `central_value` with `your_tag`.

Assuming you have followed the setup steps at the beginning of this file, there is one more step you should do:

```
export PATH=/usr/bin:${PATH}
```

You must also ensure that your options files are up-to-date. You can grab the options files from `options_files/cal`, or modify the cvmfs ones directly.

Specifically, look for / add these entries:

```
//-------Database-------//
ToolSvc.GetCalAttenuation.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.GetStripResponse.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.GetCalEnergy.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.TimingCorrections.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.BadChanTool.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.GetCalCharge.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.GetCalPE.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.GetTempResponse.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.PedSupTool.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.GetCalTime.DbUrl = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
ToolSvc.GetVetoEfficiency.DbUrl  = "https://dbdata0vm.fnal.gov:9443/mnvcon_prd/app/";
```

There is also a bit of ambiguity (or at least it arose in the context of the HNL sample) of which MINERvA geometry description is loaded by the `ReadoutAlg` of Cal stage. For safety, navigate to the options files in `MCFullChain` and add the following lines too:

```
//DetectorsHits.Members += { "GetTrackerHitsAlg/GetVetoHits"};         //uncomment to readout the Veto paddle's hits

//----- Configure the MinervaPlexModel to use the correct Plex -----//
// If this is unset it will *not* load the Veto Wall. This is bad.

ToolSvc.MinervaPlexModel.MCPlexNumberMinerva = 14;
```
Finally, if you are running over an anti-nu sample, you will need to add a line to `ProduceMC.py` to correct for the different playlist naming convention. Add anywhere in the `if opts["beam"] == "le010z185i": #matches 1,7,9,13` condition block.

```
elif opts["beam"] == "me000z-200i": #matches ME anti-neutrino
    opts["playlist"] = os.path.join( playlistDir, "playlist_minervame5A_me000z-200i_FilledH2O_EmptyHe_ovly.dat" )
```

The submission command is then:[^2]

```
mkdir -p <minerva_area>/Tools/SystemTests/runMCCal
cd <minerva_area>/Tools/SystemTests/runMCCal
python ../../ProductionScripts/mc_scripts/ProcessMC.py --overlay --cal --run <runNo> --subruns <subRange> [--nocat] [--singlenu] [--no_flux_rec] --indir=<path_to_indir> --outdir=<path_to_outdir> --ignore_warnings --os=SL6
```

If you're using a custom GENIE tag, add  an  addiional `--genie_var {Your_Genie_Var}` to the above command. Additionally, you'll want to edit `MCCalStage.py` similar to `MCGenieStage.py` by added the line at around line 269:

```
wrapfile.write("export GUSERPHYSOPT=%s\n" % self.opts["genie_var"])
```
If your GENIE var is also like the form `_Signal` (like  above with `flavor_swap_Signal`), you'll have to make the same change that you did in `MCGenieStage.py`:

```
if self.opts["genie_var"]:
      reducedOpts = self.opts["genie_var"][0:self.opts["genie_var"].find("_Signal")]
      wrapfile.write("export GUSERPHYSOPT=%s\n" % reducedOpts)
```
Some explanation of the options (for more details see the source at `/cvmfs/minerva.opensciencegrid.org/minerva/CentralizedProductionScripts/Tools/ProductionScripts/mc_scripts/ProcessMC.py`):

- `--overlay` : specifies data overlay. The options file used will be `Tools/SystemTests/options/MCFullChain/MCCal-NuMI-DataOverlay.opts`. You can force a non-overlay sample with `--force_minerva_overlay_off` instead of `--overlay`, the options file being used becoming `MCFullChain/MCCal-NuMI.opts`.

- `--run`: 1 run

- `--subruns`: can be a comma-separated list, range, or single number. E.g: 1,4-10,11

- `--nocat`: Don't use sam catalog to find input file. You almost always want this function when using your custom-made GENIE files.

- `--singlenu`: Generate one neutrino interaction per spill. You can switch to the realistic version `--multinu` to do multiple interactions per spill.

- `--no_flux_rec`: Add this if your GENIE files do **not** contain a `flux` branch, otherwise your Cal job will crash and burn.

## Common Gotchas
- Your jobs are failing when you didn't change anything in the submission: check that you refreshed the output directory. Jobs will fail if they detect files already occupying the output directory.

- Jobs are failing because they can't find the correct `param_set` in `UserPhysicsOptions.xml` (will sometimes show as an unhelpful seg fault + stack trace): delete the stored tar of your active working area in the top directory in your output area. If it's present at job submission, the script won't create a new tar and miss any changes you've made to `UserPhysicsOptions.xml`.

## Useful Links

There is general production tutorial **[here](https://cdcvs.fnal.gov/redmine/projects/minerva-sw/wiki/Tutorial_For_Production_Experts)**, a guide to files in the Inextinguishable production **[here](https://cdcvs.fnal.gov/redmine/projects/minerva-sw/wiki/Inextinguishable_Production)**, a guide to Monte Carlo run numbering **[here](https://cdcvs.fnal.gov/redmine/projects/minerva-sw/wiki/Monte_Carlo_Production_Run_Numbers)**

Inextinguishable file dCache status can be found **[here](https://minerva-exp.fnal.gov/dcachemon/#nuke)**

Logs for jobs can be accessed **[on Kibana](https:/landscape.fnal.gov/kibana/app/kibana#/dashboards)** (search for "minervapro" on Fifebatch History, find the job you want and click [stdout] or [stderr])

User batch details (e.g. efficiency, job hold events, etc) can be accessed **[on Grafana](https://fifemon.fnal.gov/monitor)**

The File Transfer Service (FTS) status website to keep track of files saved in tape can be found **[here](https://samweb.fnal.gov:8483/sam/minerva/samdftsgpvm01.fnal.gov/fts/status)**

Documentation of the metadata needed to declare files to tape lives **[here](https://cdcvs.fnal.gov/redmine/projects/sam-web/wiki/Metadata_format)**

Oscar's guide to producing DSECal samples is **[here](https://minerva-docdb.fnal.gov/cgi-bin/sso/RetrieveFile?docid=30936&filename=22-08-05ProductionTeam.pdf&version=1)** and google doc **[here](https://docs.google.com/document/d/1B5VcPDOIpwjQuWuNYI77U8ns4zwEdC7gam0k1NZZZS4/edit)**

Zubair's guide to producing anatuples is **[here](https://minerva-docdb.fnal.gov/cgi-bin/sso/RetrieveFile?docid=30939&filename=Anatuple%20production.pdf&version=1)**

John's guide to converting from Genie 3 to Genie 2 is **[here](https://minerva-docdb.fnal.gov/cgi-bin/sso/RetrieveFile?docid=30933&filename=Hacking%20GENIEv3%20to%20work%20with%20MINERvA.pdf&version=1)**

Fifemon to see how `minervapro` jobs are doing **[here](https://fifemon.fnal.gov/monitor/d/000000116/user-batch-details?orgId=1&from=now-6h&to=now&var-cluster=fifebatch&var-user=minervapro)**
