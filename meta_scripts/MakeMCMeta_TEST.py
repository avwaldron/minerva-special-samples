# Script to make SAM metadata files for Minerva Monte Carlo Files
########
#warning:  For SAM we set the run number to be the string concatenation of run+subrun.  Do not set SAM's runNumber to be the minerva run number.

OLD = True  # this turns on and off the old metadata generation

import time,os,sys,string,glob,math

import tarfile
import zipfile

from Sam import sam

from SamException        import SamExceptions

from SamFile.SamDataFile import SamDataFile
from SamFile.SamDataFile import ImportedDetectorFile
from SamFile.SamDataFile import ImportedSimulatedFile

from SamFile.SamDataFile import ApplicationFamily
from SamFile.SamDataFile import CRC
from SamFile.SamDataFile import SamTime
from SamFile.SamDataFile import RunDescriptorList
from SamFile.SamDataFile import SamSize
from SamFile.SamDataFile import LumBlockRangeList
from SamFile.SamDataFile import LumBlockRange
from SamFile.SamDataFile import CaseInsensitiveDictionary

#import MinervaLocation

def getMetaData(rawfilename):
    print "Fetching metadata for file "+rawfilename
    thismeta = sam.getMetadata(fileName=rawfilename)
    print thismeta
    return thismeta

def makeMetaData(filetype, processtype, executable, options_file, input_file, output_file, start_time, stop_time, meta_dir, n_events, run_num, eventtype, grid,genie_var="central_value", cal_var="central_value"):


    output_location = output_file

    if genie_var== "dummy":
        print "no genie variation given, make it central_value"
        genie_var = "central_value"
    if cal_var == "dummy":
        print "no cal variation given, make it central_value"
        cal_var = "central_value"
    print "*"*40
    print "Making Metadata..."
    print "  filetype     = ",filetype
    print "  processtype  = ",processtype
    print "  executable   = ",executable
    print "  options_file = ",options_file
    print "  input_file   = ",input_file
    print "  output_file  = ",output_file
    print "  start_time   = ",start_time
    print "  stop_time    = ",stop_time
    print "  meta_dir     = ",meta_dir
    print "  n_events     = ",n_events
    print "  run_num      = ",run_num
    print "  eventtype    = ",eventtype
    print "  grid         = ",grid
    print "  genie_       = ",genie_var
    print "  cal_var      = ",cal_var
    print "*"*40
    # if running on grid, find cached output to get size/crc
    # in general the main output goes to CONDOR_DIR_<PROCESS>.
    # exit files are in CONDOR_DIR_EXIT, except those made in the argo process.  dst files are in CONDOR_DIR_DST. hist files are in CONDOR_DIR_HIST
    # also, the minos output is copied to the final (nongrid) location before this script runs, so don't use CONDOR_DIR_MINOS
    #if grid=="grid" and processtype != "minos":

    # Now we want minos output is written in CONDOR_DIR_MINOS and this script need to check the file size from CONDOR_DIR_MINOS
    if grid=="grid":
        output_location = str(os.getenv("CONDOR_DIR_"+processtype.upper()))+"/"+os.path.basename(output_file)
        if processtype=="cal" :
            if (filetype=="hepevent") or (filetype=="ascii") or (filetype=="genevent" ) or (filetype=="overlaid_spill") :
                output_location = str(os.getenv("CONDOR_DIR_EXIT"))+"/"+os.path.basename(output_file)
        if filetype=="dst":
            output_location = str(os.getenv("CONDOR_DIR_DST"))+"/"+os.path.basename(output_file)
        if filetype=="hist":
            output_location = str(os.getenv("CONDOR_DIR_HIST"))+"/"+os.path.basename(output_file)
    
    #gather needed pieces before file is created to make failure more obvious
    filesize = ""
    crc = ""
    try:
      filesize = SamSize("%dB"%os.path.getsize(output_location))
      crc = str(sam.calculateFileCrc(path=output_location))
    except Exception, e:
      print "encountered exception:",e
      sys.exit("I could not find the output file, so no metadata can be produced.\n")
      
    f = open(meta_dir+"/"+os.path.basename(output_file)+".metadata.py",'w')
    f.write("from SamFile.SamDataFile import SamDataFile\n")
    f.write("from SamFile.SamDataFile import ApplicationFamily\n")
    f.write("from SamFile.SamDataFile import CRC\n")
    f.write("from SamFile.SamDataFile import SamTime\n")
    f.write("from SamFile.SamDataFile import RunDescriptorList\n")
    f.write("from SamFile.SamDataFile import SamSize\n")
    f.write("from SamFile.SamDataFile import NameOrId\n")
    f.write("import SAM\n")
    f.write("metadata=ImportedSimulatedFile({\n")
    f.write("'fileName' : '"+os.path.basename(output_file)+"',\n")
    f.write("'fileType' : 'importedSimulated',\n")
    f.write("'fileFormat' : 'root',\n")
    f.write("'fileSize' : %s,\n" % repr(filesize))
    f.write("'fileContentStatus':'good',\n")
    f.write("'crc' : "+crc+",\n")
    f.write("'eventCount' : "+n_events+"L,\n")
    f.write("'dataTier' : 'mc-"+processtype+"-"+filetype+"',\n")
    f.write("'firstEvent' : 1L,\n")
    f.write("'lastEvent' : "+n_events+"L,\n")

    f.write("'startTime' : "+str(SamTime(str(start_time),"%Y-%m-%d_%H:%M:%S").inSeconds())+",\n")
    f.write("'endTime' : "+str(SamTime(str(stop_time),"%Y-%m-%d_%H:%M:%S").inSeconds())+",\n")
    f.write("'applicationFamily' : ApplicationFamily(appFamily='mc', appName='"+processtype+"', appVersion='"+str(os.environ.get("MINERVA_RELEASE")).lower()+"'),\n")
    f.write("'group' : 'minerva',\n")
    f.write("'params' : Params({'MC' : {'MCCalVariationTag' : '%s','MCGeneratorVariationTag' : '%s' }} ),\n" %(cal_var,genie_var) )
    f.write("'datastream': 'mc',\n")
    runtype = 'minerva'
    if output_location.find("downstream")>=0: runtype='downstream'
    if output_location.find("frozen")>=0: runtype='downstream'
    if output_location.find("prototype")>=0: runtype='prototype'
    f.write("'runDescriptorList':RunDescriptorList([RunDescriptor(runType='"+runtype+"',runNumber="+run_num+")]),\n")
    f.write("})")
    f.close()
    
def declareMetaData(recoMetaFile):
    print "recoMetaFile"+recoMetaFile
    try:
        sam.declareFile(args=recoMetaFile)
        print "file "+os.path.basename(recoMetaFile)+" declared to sam "
    except SamExceptions.SamException, x:
        print 'SAM declare failed: %s' % str(x)

def undeclareMetaData(recoMetaFile):
    basename = os.path.basename(recoMetaFile[0:-12])
    try:
        sam.undeclareFile(args=basename)
        print "file "+os.path.basename(basename)+" undeclared from sam "
    except SamExceptions.SamException, x:
        print 'SAM declare failed: %s' % str(x)

def verifyMetaData(recoMeta):
    try:
        sam.verifyMetadata(metadata=recoMeta,d=1)
    except SamExceptions.SamException, x:        
        print 'SAM verify failed: %s' % str(x)

def locateMetaData(filename) :

    connectstring=connect(os.path.expanduser("~ljf26/private/samadmin"))
    sampath= string.split(mydir,sambase)[1]
    if(sampath[0]=="/"):
        sampath = sampath[1:]

    # first see if file is in SAM
    md = "None"
    try:
        md = sam.getMetadata(fileName=filename)
    except SamExceptions.SamException, x:
        print 'SAM failed: %s' % str(x)
           
    except SamExceptions.DataFileNotFound:
        print 'No file in SAM named %s' % name

    if md =="None":
        print name," not declared in sam"
        return

    # now try to locate it
    try:
        locs = sam.locate(args=filename)
        for loc in locs:
            where = loc.getFullPath()
            if(string.find(where,mydir)>-1):
                print "file", name," already has this location"
                return
    except SamExceptions.SamException, x:
        print x

    # make the location
    x = "none"
    try:
        samAdmin.addDiskLocation(MountPoint=samdisk,RelativePath=sampath,connect=connectstring)
    except SamExceptions.SamException, x:
        print 'SAM tried to add a location: %s' % str(x)   
        if(string.find(str(x),"already registered") < 0):
            print "could not add location ", samdisk, sampath
            return
        else:
            print " location ",samdisk+sampath,"already exists"
        cleanup = 0
        if cleanup:    
            try:
                print "just before cleanup ", samdisk, sampath
                sam.eraseFileLocation(fileName=name,replicaLocation=samdisk+"//"+sampath)
            
            except SamExceptions.SamException, x:
                print 'SAM failedto erase a file to a  location: %s, %s' % (str(x),name)
        try:
           
            sam.addLocation(fileName=name,replicaLocation=samdisk+"/"+sampath)
            
        except SamExceptions.SamException, x:
            print 'SAM failedto add a file to a  location: %s, %s' % (str(x),name)   
        try:
            location = sam.locate(args=name)
            print "file ",name," added to location ",mydir
        except SamExceptions.SamException, x:
                print 'SAM tried to find file %s: %s' % (name,str(x))
                return


if __name__ == '__main__':

    if len(sys.argv) < 14 :
        print "Usage: sampy MakeMCMeta.py <filetype> <processtype> <executable> <options> <input file> <output file> <starttime> <stoptime> <meta dir> <n_events> <sam_run_num> <eventtype> <usegrid> <genie variation> <cal variation>"
        print "arguments found: "+str(sys.argv)
        sys.exit(2)

    filetype = sys.argv[1]
    processtype = sys.argv[2]
    executable = sys.argv[3]
    options_file = sys.argv[4]
    input_file = sys.argv[5]
    output_file = sys.argv[6]
    start_time = sys.argv[7]
    stop_time = sys.argv[8]
    meta_dir = sys.argv[9]
    n_events = sys.argv[10]
    run_num = sys.argv[11]  #note: this is a string of <run><subrun>, not the minerva-defined run number
    eventtype = sys.argv[12]
    usegrid = sys.argv[13]
    genie_var = sys.argv[14]
    cal_var = sys.argv[15]

    # this check creates problems when running on condor (creation location is not final location)
    # turn off for now and assume output file exists
    ## make sure output file exists
    #if(not os.path.isfile(output_file)):
    #    print "Output file does not exist.  No metadata will be made."
    #    sys.exit()
    
    # if checks are okay, make the metadata and declare to SAM

    if OLD:
        recoMeta = makeMetaData(filetype, processtype, executable, options_file, input_file, output_file, start_time, stop_time, meta_dir, n_events, run_num, eventtype, usegrid,genie_var,cal_var)

    arglist = ""
    for args in sys.argv[1:]:
        arglist += args + " " 

    command = "python ./MakeMCJson_TEST.py " + arglist
    #"python $PRODUCTIONSCRIPTSROOT/mc_scripts/MakeMCJson.py " +arglist
    print command
    try:
        print " Make the Json metadata as well"
        output = os.popen(command).readlines()
        for line in output:
            print line
    except Exception, e:
        print " error returned by MakeMCJson.py ",e
        
    #verifyMetaData(dstMeta)
    #metaFile = writeMetaData(recoMeta,filetype)
    #declareMetaData(metaFile)
    #locateMetaData(metaFile)
