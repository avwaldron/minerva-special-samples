#!/usr/bin/bash

FILETYPE="pool"
PROCESSTYPE="genie"
OPTIONS="none"
INPUTFILE="none"
OUTPUTFILE="BADOUTPUT"
STARTTIME="BADSTART"
STOPTIME="BADSTOP"
METADIR="BADMETADIR"
NEVTS=1000
RUNNO="BADRUN"
EVTTYPE="mixed"
GRIDUSE="nogrid"
GENIE_VAR="HNL"
#"Sammy"
#"HNL"
EXECUTABLE="/minerva/app/users/minervapro/cmtuser/Minerva_v21r1p1_HNL/GENIE/x86_64-slc6-gcc49-opt/GENIE/bin/gevgen"
CAL_VAR="dummy"

function buildMetadata ()
{
    #echo $1
    OUTPUTFILE=$1
    #echo

    # separate out the base filename at the end from the directory
    BASE_FNAME=${1:$(echo "${#1}" - 50 - "${#GENIE_VAR}" | bc)}
    echo "BASE_FNAME: ${BASE_FNAME}"
    FULL_FPATH=${1:0:$(echo "${#1} - ${#BASE_FNAME}" | bc)}
    #echo "FULL_FPATH: ${FULL_FPATH}"

    # isolate run. SAM wants run + subrun concatentated

    RUNNO=${FULL_FPATH:$(echo "${#FULL_FPATH}" - 9 | bc):2}
    RUNNO=${RUNNO}${FULL_FPATH:$(echo "${#FULL_FPATH}" - 6 | bc):2}
    RUNNO=${RUNNO}${FULL_FPATH:$(echo "${#FULL_FPATH}" - 3 | bc):2}
    RUNDIR=${FULL_FPATH:$(echo "${#FULL_FPATH}" - 12 | bc)}

    SUBNO=${BASE_FNAME:$(echo "${#BASE_FNAME} - 29 - ${#GENIE_VAR}" | bc):4}
    RUNNO=${RUNNO}${SUBNO}

    #echo "RUNDIR: ${RUNDIR}, RUNNO: ${RUNNO}"
    
    # metadir is simple. Replace "/genie/" with "/genie_meta/, truncate" and mkdir the dir if not exists.
    #echo
    METADIR=${FULL_FPATH//\/genie\///genie_meta/}
    #echo "METADIR: ${METADIR}"

    # finally, build start and end times. 
    # end will be NOW
    # start will be 3 minutes before NOW
    #echo
    STOPTIME=$(date '+%Y-%m-%d_%H:%M:%S')
    STOPONE=$(date '+%Y-%m-%d_%H:')
    STOPMINS=$(date '+%M')
    if [ ${STOPMINS} -lt 3 ]
    then
	STOPMINS=3
    fi
    STOPTWO=$(date '+:%S')
    STARTTIME=${STOPONE}$(echo "${STOPMINS} -3" | bc)${STOPTWO}
    #echo "STARTTIME : ${STARTTIME}"
    #echo "STOPTIME : ${STOPTIME}"
}

LOCATION_BASE="./SAMTEST"
LOCATION_PATH="${LOCATION_BASE}/nogrid/${GENIE_VAR}/minerva/v21r1p1/genie"
#"${LOCATION_BASE}/nogrid/HNL/minerva/v21r1p1/genie"
LOCATION_SMALL="${LOCATION_PATH}/00/11/10/00/"

for i in $(find ${LOCATION_PATH} -iname "*-${GENIE_VAR}_ghep.root")
do
    #echo ==========================
    #echo
    buildMetadata ${i}
    #echo
    #echo ==========================

    sampy MakeMCMeta_TEST.py ${FILETYPE} ${PROCESSTYPE} ${EXECUTABLE} ${OPTIONS} ${INPUTFILE} ${OUTPUTFILE} ${STARTTIME} ${STOPTIME} ${METADIR} ${NEVTS} ${RUNNO} ${EVTTYPE} ${GRIDUSE} ${GENIE_VAR} ${CAL_VAR} > ./tmp.txt
done
