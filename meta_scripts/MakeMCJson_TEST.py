# Script to make SAM metadata files for Minerva Monte Carlo Files
########
#warning:  For SAM we set the run number to be the string concatenation of run+subrun.  Do not set SAM's runNumber to be the minerva run number.

import time,os,sys,string,glob,math
import samweb_client
import json

from samweb_client import utility
import MinervaSamwebUtilities
import MinervaSamwebUtilitiesDEV

samweb = samweb_client.SAMWebClient(experiment='minerva')

import getNEvents
import mergeMeta # just for time formatting


DEBUG = True


def getMetaData(rawfilename):
    print "Fetching metadata for file "+rawfilename
    thismeta = samweb.getMetadata(rawfilename)
    print thismeta
    return thismeta

def makeMetaData(filetype, process_type, executable, options_file, input_file, output_file, start_time, stop_time, meta_dir, n_events, samrun, eventtype, grid,genie_var="central_value", cal_var="central_value"):

    if DEBUG and False:
        for key in list(os.environ.keys()):
            print"ENV: ", key, os.getenv(key)

    run = string.atoi(samrun)/10000
    subrun = string.atoi(samrun)%10000

    output_location = output_file

    if genie_var== "dummy":
        print "no genie variation given, make it central_value"
        genie_var = "central_value"
    if cal_var == "dummy":
        print "no cal variation given, make it central_value"
        cal_var = "central_value"
    print "*"*40
    print "Making Metadata..."
    print "  filetype     = ",filetype
    print "  processtype  = ",processtype
    print "  executable   = ",executable
    print "  options_file = ",options_file
    print "  input_file   = ",input_file
    print "  output_file  = ",output_file
    print "  start_time   = ",start_time
    print "  stop_time    = ",stop_time
    print "  meta_dir     = ",meta_dir
    print "  n_events     = ",n_events
    print "  run_num      = ",run_num
    print "  eventtype    = ",eventtype
    print "  grid         = ",grid
    print "  genie_       = ",genie_var
    print "  cal_var      = ",cal_var
    print "*"*40

    # if running on grid, find cached output to get size/crc
    # in general the main output goes to CONDOR_DIR_<PROCESS>.
    # exit files are in CONDOR_DIR_EXIT, except those made in the argo process.  dst files are in CONDOR_DIR_DST. hist files are in CONDOR_DIR_HIST
    # also, the minos output is copied to the final (nongrid) location before this script runs, so don't use CONDOR_DIR_MINOS
    
    if grid=="grid" and processtype != "minos":
        output_location = str(os.getenv("CONDOR_DIR_"+processtype.upper()))+"/"+os.path.basename(output_file)
        if processtype=="cal" :
            if (filetype=="hepevent") or (filetype=="ascii") or (filetype=="genevent") or (filetype=="overlaid_spill"):
                output_location = str(os.getenv("CONDOR_DIR_EXIT"))+"/"+os.path.basename(output_file)
        if filetype=="dst":
            output_location = str(os.getenv("CONDOR_DIR_DST"))+"/"+os.path.basename(output_file)
        if filetype=="hist":
            output_location = str(os.getenv("CONDOR_DIR_HIST"))+"/"+os.path.basename(output_file)
    
    #gather needed pieces before file is created to make failure more obvious
    filesize = ""
    crc = ""
    print "The output location is", output_location
    try:
      filesize = os.path.getsize(output_location)
      crc = utility.fileEnstoreChecksum(output_location) 
    except Exception, e:
      print "encountered exception:",e
      sys.exit("I could not find the output file, so no metadata can be produced.\n")

    meta = {}

    meta["file_name"] = os.path.basename(output_file)
    meta["file_type"] = "importedSimulated"
    meta["file_format"] = "root"
    meta["file_size"] = filesize
    meta["content_status"] = "good"
    meta["crc"] = crc

    datatier = "mc-"+processtype+"-"+filetype
    meta["data_tier"] = datatier
    if n_events <= 0 or n_events == "0":
        n_events = getNEvents.getNEvents(output_file,datatier)
    meta["event_count"] = n_events
    meta["first_event"] = 1
    meta["last_event"] = n_events
    parentname = os.path.basename(input_file)
   
      
    f = open(meta_dir+"/"+os.path.basename(output_file)+".json",'w')
    #f = open("./tmpJson.json",'w')


#    f.write("from SamFile.SamDataFile import SamDataFile\n")
#    f.write("from SamFile.SamDataFile import ApplicationFamily\n")
#    f.write("from SamFile.SamDataFile import CRC\n")
#    f.write("from SamFile.SamDataFile import SamTime\n")
#    f.write("from SamFile.SamDataFile import RunDescriptorList\n")
#    f.write("from SamFile.SamDataFile import SamSize\n")
#    f.write("from SamFile.SamDataFile import NameOrId\n")
#    f.write("import SAM\n")
#    f.write("metadata=ImportedSimulatedFile({\n")
#    f.write("'fileName' : '"+os.path.basename(output_file)+"',\n")
#    f.write("'fileType' : 'importedSimulated',\n")
#    f.write("'fileFormat' : 'root',\n")
#    f.write("'fileSize' : %s,\n" % repr(filesize))
#    f.write("'fileContentStatus':'good',\n")
#    f.write("'crc' : "+crc+",\n")
#    f.write("'eventCount' : "+n_events+"L,\n")
#    f.write("'dataTier' : 'mc-"+processtype+"-"+filetype+"',\n")
#    f.write("'firstEvent' : 1L,\n")
#    f.write("'lastEvent' : "+n_events+"L,\n")

#    f.write("'startTime' : "+str(SamTime(str(start_time),"%Y-%m-%d_%H:%M:%S").inSeconds())+",\n")
#    f.write("'endTime' : "+str(SamTime(str(stop_time),"%Y-%m-%d_%H:%M:%S").inSeconds())+",\n")
    timeFormat = "%Y-%m-%d_%H:%M:%S"
    startTstamp= time.strptime(start_time,timeFormat)
    meta["start_time"]=int(time.mktime(startTstamp))
    stopTstamp= time.strptime(stop_time,timeFormat)
    meta["end_time"]=int(time.mktime(stopTstamp))
#    meta["start_time"] = start_time
#    meta["end_time"] = stop_time
    meta["application"]={"family": "mc","name":process_type,"version":str(os.getenv("MINERVA_RELEASE")).lower()}
    meta["group"]="minerva"
    meta["MC.MCCalVariationTag"] = cal_var
    meta["MC.MCGeneratorVariationTag"]=genie_var
    meta["data_stream"] = "mc"
#    f.write("'applicationFamily' : ApplicationFamily(appFamily='mc', appName='"+processtype+"', appVersion='"+str(os.environ.get("MINERVA_RELEASE")).lower()+"'),\n")
#    f.write("'group' : 'minerva',\n")
#    f.write("'params' : Params({'MC' : {'MCCalVariationTag' : '%s','MCGeneratorVariationTag' : '%s' }} ),\n" %(genie_var,cal_var) )
#    f.write("'datastream': 'mc',\n")
    runtype = 'minerva'
    if output_location.find("downstream")>=0: runtype='downstream'
    if output_location.find("frozen")>=0: runtype='downstream'
    if output_location.find("prototype")>=0: runtype='prototype'

    # f.write("'runDescriptorList':RunDescriptorList([RunDescriptor(runType='"+runtype+"',runNumber="+run_num+")]),\n")
    meta["runs"] = [[ run, subrun, runtype]]

#    samweb.validateFileMetadata(meta)
# now add the parents which may not actually be there yet.  This is so you can pach the ancestry later

    #meta["parents"]           = [parentname]
    print " validate the metadata "
    try:
        samweb.validateFileMetadata(meta)
        print "samweb validation succeeded!"
    except Exception, e:
        print "samweb validation has failed", e

    json.dump(meta,f,sort_keys=True, indent=4,separators=(',',': '))
    f.close()

    print "Exiting now! Goodbye!"
    exit(0)




if __name__ == '__main__':

    if len(sys.argv) < 14 :
        print "Usage: python MakeMCJson.py <filetype> <processtype> <executable> <options> <input file> <output file> <starttime> <stoptime> <meta dir> <n_events> <sam_run_num> <eventtype> <usegrid> <genie variation> <cal variation>"
        print "arguments found: "+str(sys.argv)
        sys.exit(2)

    filetype = sys.argv[1]
    processtype = sys.argv[2]
    executable = sys.argv[3]
    options_file = sys.argv[4]
    input_file = sys.argv[5]
    output_file = sys.argv[6]
    start_time = sys.argv[7]
    stop_time = sys.argv[8]
    meta_dir = sys.argv[9]
    n_events = sys.argv[10]
    run_num = sys.argv[11]  #note: this is a string of <run><subrun>, not the minerva-defined run number
    eventtype = sys.argv[12]
    usegrid = sys.argv[13]
    genie_var = sys.argv[14]
    cal_var = sys.argv[15]

    # this check creates problems when running on condor (creation location is not final location)
    # turn off for now and assume output file exists
    ## make sure output file exists
    #if(not os.path.isfile(output_file)):
    #    print "Output file does not exist.  No metadata will be made."
    #    sys.exit()
    
    # if checks are okay, make the metadata and declare to SAM
    recoMeta = makeMetaData(filetype, processtype, executable, options_file, input_file, output_file, start_time, stop_time, meta_dir, n_events, run_num, eventtype, usegrid,genie_var,cal_var)
    print " MakeMCJson.py is done ",  recoMeta
#    samweb.validateFileMetadata(recoMeta)
    #metaFile = writeMetaData(recoMeta,filetype)
    #declareMetaData(metaFile)
    #locateMetaData(metaFile)
