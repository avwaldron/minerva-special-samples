######################################################
##
## declare2FTS.py
##
## Author: Laura Fields
## modified to move files to dcache dropbox
## HMS 14 April 2014
##
## Modified to test by John Plows -- DO NOT USE THIS FILE
#
#  syntax is "python declare2FTS.py <dir>
# anything in that directory that can be declared will be
# and put in the dropbox for storage to tape.
#
## Date: 7 March 2011
## 
## Looks for processed data not yet declared to SAM
## and declares it
##
#######################################################

import os,time,sys,datetime, glob, fnmatch,string,subprocess, json

import MinervaSamwebUtilities
import samweb_client
samweb = samweb_client.SAMWebClient(experiment='minerva')

import dataTierMap

#############################################
## Main Function
#############################################


DEBUG = True

VERBOSE = True

IFDH = False # this turns on using copy instead of link to move to dropbox

types = ["cal","reco","minos","ana","genie","mnv_exit"] # remove hist and dst

# list of bad files with no metadata
badlist = ["detsim.histman.root"]

print "Populating listApplications()"
samAppList = samweb.listApplications()
print "...Done!"

if __name__ == '__main__':
  
    dropbox = "/pnfs/minerva/scratch/users/kplows/dropbox/"
    #"/pnfs/minerva/scratch/users/minervasam/dropbox/"

    if len(sys.argv) < 3:
        print "must provide the top level directory for what you want to store"
        print "python declare2FTS_TESTJOHN.py <test or store> <dir>"
        sys.exit(1)

    
    dir = sys.argv[2]
    print "Running declare2FTS_TESTJOHN.py with args " + sys.argv[1] + " " + sys.argv[2]
    TEST = (sys.argv[1] == "test")
    if TEST:
        #DEBUG=True
        DEBUG=False
    if not TEST: #and sys.argv[1] != "store":
        print "Store has been deactivated for now."
        #print "the first argument needs to be test or store"
        sys.exit(1)

    if dir.find("/pnfs/minerva/scratch/") < 0 and dir.find("/minerva/data/" )<0:
        print " needs to be in the /pnfs/minerva/scratch or /minerva/data tree"
        sys.exit(1)

    start = datetime.datetime.now()
    
    # if files are on disk, use the old bluearc dropbox

    BLUE = False
    if dir.find("/minerva/data") == 0:
        dropbox = "/minerva/data/users/kplows/fts_upload"
        #"/minerva/data2/fts_upload/"
        BLUE = True
        
    cmd = "find  "+dir + "| sort"
    result = os.popen(cmd).readlines()
    directory_list=result

    if directory_list == "":
        print "Cound not find a recent directory listing of " + dir
        sys.exit(1)

    print "Obtaining  tree from",dir

    # Look through processed data
    #f = open(directory_list)
    lines = directory_list
    #f.close

    print "Found",len(lines),"files and directories in dir";

    n_files = 0;
    counter = 0;
    filecounter=0;
    storecounter = 0
    time0=time.time()
    #print "Start Timer at " + time.strftime("%a, %d %b %Y, %H:%M:%S", time.gmtime(time0))
    time1=time0
    printInterval=20

    for line in lines:
        counter += 1;
        if storecounter > 20000:
            print " have done 20000 files, run me again to do more"
            break
	
        if counter % 100 == 0: 
	    print "Lines scanned: %s"%( counter )
	    timeNow = time.time()
	    dt0 = timeNow - time0
	    print "Total Time Elapsed: %ds"%(dt0)

        datapath = line.strip()
#        print datapath
        if not datapath.endswith("root") and not datapath.endswith("dat"):
            continue;
	filecounter+=1

        name = os.path.basename(datapath)
        if DEBUG:
            print name
        now = datetime.datetime.now()
        if (now-start).seconds > 60*60*18: # 18 hours
            print "Job has exceeded maximum time length... quitting"
            print "Processed",n_files,"files."
            sys.exit()
        bad = False
        for badname in badlist:
            if name.find(badname)> -1:
                if VERBOSE:
                    print " skip file with no metadata",name,badname
                bad = True
                break
        if bad:
            continue
        metapath = datapath
        metapath = metapath.replace(".root",".root.json")
        metapath = metapath.replace(".dat",".dat.json")
        metapath = metapath.replace(".dat.json/",".dat/") # fix case where .dat is in the path
        if metapath.find("GENIE") > -1 and metapath.find("ghep") == -1:
            if DEBUG:
                print " skip non-ghep GENIE file"
            continue
        for typename in types:
            #if DEBUG:
            #    print "Debug"
            type = "/"+typename+"/"
            metapath = metapath.replace(type,"/"+typename+"_meta/")
            #print "before",  metapath
#        print string.find(metapath,"/pnfs/minerva/scratch")
        if DEBUG and filecounter % printInterval == 0:
            print metapath
        #metapath = string.replace(metapath, "/pnfs/minerva/scratch","/minerva/data")
        #print "after", metapath
        if not os.path.exists(metapath):
            if DEBUG:
                print " no metadata exists",datapath,metapath
            continue
        
        metaname = os.path.basename(metapath)
        dataname = os.path.basename(datapath)
        #print metapath
        declaredname = metapath+".declared"
        if os.path.exists(declaredname) and not TEST:
            if VERBOSE:
                print "Perhaps already declared: ",metaname
            #continue

        if filecounter % printInterval == 0:
	    print "=================================================="
	    print "Files Processed: %d"%filecounter
	    timeNow = time.time()
	    dt0 = timeNow - time0
	    dt1 = timeNow - time1
	    time1 = timeNow
	    print "Total Time Elapsed: %s h"%(datetime.timedelta(seconds=int(dt0) ))
	    print "Time Elapsed per %d Files: %ds"%(printInterval, dt1)
	    print "Avg FPM is : %.2f/min"%(1.* filecounter/dt0*60 )
	    print "Avg FPM per %d files is:%.2f/min"%(printInterval, 1.*printInterval/dt1*60) 
	    print "=================================================="

        if DEBUG:
            print "Attempt to declare and store  metafile ",metapath

        # calculate file age in hours; don't load really new files
        #age = int((time.time() - os.path.getmtime(metapath))/60/60)            
        #if age < 4:
        #    continue

        if not os.path.exists(datapath):
            print " no data"
            continue

        # Temporary hack to ignore v10r4p4 histogram files.  This can be
        # be removed once v10r4p4 files are declared
        if datapath.startswith("/minerva/data2/users") or datapath.startswith("/minerva/data/users/"):
            if datapath.find("histo")>=0:
                continue
        
        

        #n_files = n_files + 1
        #if n_files > 1: break
        #print n_files

        # Parse metadata to find any parents this data has
        # Look for parents; don't declare if parents aren't declared
        parents_in_sam = True 
        f = open(metapath);
        metadata = json.load(f)
        if MinervaSamwebUtilities.isDeclared(dataname):
            if DEBUG:
                print "using declared metadata"
            metadata = samweb.getMetadata(dataname)
        data_tier = metadata["data_tier"]

        new_data_tier = data_tier.replace("cal-pool-user","calibrated-pool")
        metadata["data_tier"] = new_data_tier
        data_tier = new_data_tier

        '''
        if metapath.find("GENIE") > -1:
            genie_version = metadata["application"]["version"]
            filename = metadata["file_name"]
		    newVersion = genie_version.replace("v10r8p9", "v10r8p4")
		    metadata["application"]["version"] = newVersion
		    #metadata["file_name"] = newFilename
        '''

        #if DEBUG:
            #print metadata
        if metadata.has_key("parents") and metadata["parents"] == ["none"]:
             metadata.pop("parents",None)
             if DEBUG:
                 print " removing parents=none"    
 
        if metadata.has_key("parents"):
            parents = metadata["parents"]
            for parent in parents:
                #print "parents", parents
                if not isinstance(parent, basestring) and parent.has_key("file_name"):
                    if not MinervaSamwebUtilities.isDeclared(parent['file_name']):
                        parents_in_sam = False
                else:
                    if not MinervaSamwebUtilities.isDeclared(string.strip(parent)):
                        parents_in_sam = False
                f.close()
       
        if not parents_in_sam:
            print "WARNING: A parent of",dataname,"is not declared in SAM.  Skipping this file."
	    continue
        if DEBUG and filecounter % printInterval == 0:
            print "ready to declare and link",metapath

        MinervaSamwebUtilities.addApplicationFamilyFileList(metapath, samAppList);
        if not MinervaSamwebUtilities.isDeclared(dataname):
	    print metadata["data_tier"]
            if metadata.has_key("user") and metadata["user"] != os.getenv("USER"):
                print "changing username so I can store",metadata["user"]," --> ", os.getenv("USER")
                metadata["user"] = os.getenv("USER")
                
            if not TEST:
                if VERBOSE:
                    print "declare ",os.path.basename(metapath)
                try:
		    t0=time.time()
                    samweb.declareFile(metadata);
		    print "declareFile Time, t: %.5fs"%(time.time()-t0)
                except Exception, e:
                    print " metadata could not be declared", e
                    sys.exit(1)
            else:
                try:
                    samweb.validateFileMetadata(metadata)
                    if( filecounter % printInterval == 0 ):
                        print "would have declared",metapath
                except Exception,e:

                    print " metadata is invalid, please fix",e
                    sys.exit(1)
        #md = sam.getMetadata(fileName=dataname)
        #data_tier = md['dataTier']
# try to get the metadata before declaration
        BAD = False
        # check to see if it already has a tape location

        try:
            location = MinervaSamwebUtilities.getFileLocation(dataname,"pnfs")
            if location != "none":
                if VERBOSE:
                    print "file already in pnfs",location
                continue
            #print "location is ",location
        except Exception, x:
            if(False):
                print " could not check location"

        try:
        
            filesize = os.path.getsize(datapath)
            size = metadata["file_size"]
           
            DIFF = 0
            if size - filesize > DIFF or filesize - size > DIFF:
                BAD = True
                print "filesize mismatch - cannot store this file: true = ",filesize,size," meta = \n python $PRODUCTIONSCRIPTSROOT/keepup_scripts/fixDropbox.py ",dataname,os.path.dirname(datapath)  
                try:
                    print " try to fix the filesize"
                    cmd = " python $PRODUCTIONSCRIPTSROOT/keepup_scripts/fixDropbox.py %s %s" % (dataname,os.path.dirname(datapath))	
                    print cmd
                    os.system(cmd) 
                    #fixTapeSize.fix(dataname,os.path.dirname(datapath))

                except Exception, e:
                    print " attempt to fix file size failed ",dataname
                continue

        except Exception, x:
            print "could not import metadata file ",x
            continue

    print "I am exiting now with SUCCESS. Goodbye!"

            
   
