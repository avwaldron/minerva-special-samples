#!/bin/env python

from MCBaseStage import MCBaseStage
import os, sys, glob, shutil, re
import PNFSUtils

import ifdh
ifdh_handle = ifdh.ifdh()

class MCRecoStage(MCBaseStage):
  """Knows how to run the Reco stage of MC production"""
  def __init__(self, opts):
    MCBaseStage.__init__(self,opts)#base constructor
    self.exec_flag = "--reco"
    self.my_tag = "Reco"
    self.my_pool_out = self.reco_out

    if not self.opts["no_condor"]:
      self.newSYSTEMTESTROOT = "%s/%s/%s" % ("$CONDOR_DIR_INPUT",os.getenv("SYSTEMTESTSROOT").split("/")[-3],"Tools/SystemTests")
    else:
      self.newSYSTEMTESTROOT = os.getenv("SYSTEMTESTSROOT")

    self.executable = "%s/%s/SystemTestsApp.exe" % (self.newSYSTEMTESTROOT, os.getenv("CMTCONFIG"))

    self.my_opts_template = self.opts["opts_file"]
    if self.my_opts_template == "dummy":
      self.my_opts_template = "%s/options/MCFullChain/MCReco_fCalDigits.opts" % os.getenv("SYSTEMTESTSROOT")

    #these will be set during prepare subrun
    self.cal_input = ""
    self.minos_input = ""

  def prepareSubrun( self, sub, usecat=False ):
    MCBaseStage.prepareSubrun( self, sub )
    if not usecat:
      if(not self.opts["genie_var"]):
        self.cal_input = "%s/%s_%s_%s.root" % ( self.cal_in, self.prefix, self.cal_tag, self.suffix_in )
      else:
        hGVar = "-"+self.opts["genie_var"]
        hGVIndex = self.suffix_in.find(hGVar)
        self.suffix_in = self.suffix_in[0:hGVIndex]
        self.suffix_in = self.suffix_in + "_" + self.opts["genie_var"]
        self.cal_input = "%s/%s_%s_%s.root" % ( self.cal_in, self.prefix, self.cal_tag, self.suffix_in )
    else:
      subrun = MCBaseStage.findInputFilesFromSAM( self, sub )
      #if len(subrun)!=1:
      #  print 'ERROR finding files in sam:', self, sub, subrun
      #  sys.exit(1)
      if len(subrun) > 0:
        self.cal_input = subrun[0]
      else:
        print 'ERROR finding files in sam: ',sub
#      self.cal_input = subrun[0]

    if self.opts["use_argo"]:#argoneut adds a tag to the filename
      self.minos_input = "%s/%s_%s_%s.sntp.root" % ( self.minos_in, self.prefix, self.argo_tag, self.suffix_in )
    else:
      self.minos_input = "%s/%s_%s_%s.sntp.root" % ( self.minos_in, self.prefix, self.exit_tag, self.suffix_in )
    if usecat:
      minos_dir = os.path.dirname(self.cal_input).replace("mc_processing_cal/mc-cal-pool","mc_minos/mc-minos-sntp")
      self.minos_input = os.path.join(minos_dir,os.path.basename(self.minos_input))
      print "minos input is ",self.minos_input
    
    if self.opts["no_minos"]:
      self.minos_input = "none"

  def inputIsGood( self ):
    """Check that the necessary Cal and MINOS files exist."""
    if not os.path.isfile(self.cal_input):
      print "Cannot find the Cal input file.  I looked for '%s'" % self.cal_input
      return False
    if not self.opts["no_minos"] and not os.path.isfile(self.minos_input):
      print "Cannot find the MINOS input file.  I looked for '%s'" % self.minos_input
      print "I'm not going to lie to you, this is bad and I'm quitting"
      return False
    return True

  def execute( self ):
    """Implementation of the execute method for Reco.  Create directories, generate the Reco options file, then use MCBaseStage to run."""
    if len(self.my_dirs) == 0:
      self.prepareDirs()
    self.prepareOpts()
    self.prepareExecutable()
    MCBaseStage.execute( self )

  def prepareDirs( self ):
    """Make the directories that the Reco stage needs.  Define the directories for condor."""
    self.my_dirs.append( self.opts_out )
    self.my_dirs.append( self.scripts_out )
    self.my_dirs.append( self.logs_out )

    self.my_dirs.append( self.reco_out )
    self.my_dirs.append( self.hist_out )
    self.my_dirs.append( self.pot_out )
    if self.opts["do_dst"]:
      self.my_dirs.append( self.dst_out )


    if self.opts["make_meta"]:
      self.my_dirs.append( self.reco_meta )
      self.my_dirs.append( self.hist_meta )
      if self.opts["do_dst"]:
        self.my_dirs.append( self.dst_meta )

    self.makeDirs()

    if not self.opts["no_condor"]:  #create the condor directory maps unless running interactively
      self.addCondorDir("RECO", self.reco_out )
      self.addCondorDir("HIST", self.hist_out )
      self.addCondorDir("POT",  self.pot_out )
      if self.opts["do_dst"]:
        self.addCondorDir("DST", self.dst_out )
      if self.opts["make_meta"]:
        self.addCondorDir("RECOMETA", self.reco_meta )
        self.addCondorDir("HISTMETA", self.hist_meta )
        if self.opts["do_dst"]:
          self.addCondorDir("DSTMETA", self.dst_meta )

  def prepareOpts( self ):
    """Generate the options file that is used to run the Cal stage Gaudi job.  Define executable and executableArgs."""
    self.logfile = "%s/MCReco-%s_%s.log" % (self.logs_out, self.prefix, self.suffix_out)
    self.my_opts_filename = "%s/MCReco-%s_%s.opts" % (self.opts_out, self.prefix, self.suffix_out)

    #copy lines from the template, replacing as needed
#    optsnew = open(self.my_opts_filename,"w")
    optsnew = open("tmp.opts","w")
    versionFile = os.environ['LOCALPRODUCTIONSCRIPTSROOT'] + '/versioning/current_version.txt'
    productionScriptsVersion = open(versionFile).readlines()[0]
    optsnew.write("//============================================================\n")
    optsnew.write("// This job was processed using CentralizedProductionScripts version %s\n" % productionScriptsVersion)
    optsnew.write("// To see details, consult $PRODUCTIONSCRIPTSROOT/versioning/CentralizedProductionScriptsReference_version%s.txt\n" % productionScriptsVersion)
    optsnew.write("//============================================================\n")
    for lineold in self.expandOptsFile( self.my_opts_template ):
      lineold = lineold.strip()
      linenew = self.swapOptsLine(lineold)

      # getInputFileString knows whether to give grid or interactive names, and takes a list
      linenew = re.sub(r'(^EventSelector\.Input).*', self.getInputFileString([self.cal_input]), linenew)
      minos_string=self.minos_input if self.opts["no_condor"] else self.getCondorFileName(self.minos_input)
      linenew = re.sub(r'(^MinosDataAlg\.MinosFiles).*',r'\1 = {"%s"};' % minos_string, linenew )

      #TEMP TEMP TEMP (for v10r6p1): Do no load OD into reconstruction geometry
      #StateCorrection should not know about the OD because didn't load the OD in v10r6.
      #There's no real problem, but results in tracking might be slightly different if we don't exlucd the OD.
      linenew = re.sub(r'^(Geo\.StreamItems.*/dd/Structure/Minerva/Detector/OuterDetector.*)', r'//\1', linenew)

      optsnew.write(linenew + "\n")

    try:
      optsnew.close()
      print "Copying temporary options file to outdir", self.my_opts_filename.replace("//","/")
      cp_arg=["%s/tmp.opts"%(os.environ["PWD"]),self.my_opts_filename.replace("//","/")]
      ifdh_handle.cp(cp_arg)
      os.system("rm tmp.opts")
    except:
      os.system("rm tmp.opts")
      sys.exit("Failed to copy %s. Bailing" % self.my_opts_filename)



  def prepareExecutable( self, tbname ):
    """Generate the script that runs the Cal job.  Define executable and executableArgs."""
    self.logfile = "%s/MCReco-%s_%s.log" % (self.logs_out, self.prefix, self.suffix_out)
    self.wrapper = "%s/MCReco_wrapper-%s_%s.sh" % (self.scripts_out, self.prefix, self.suffix_out)
    self.executableArgs = "$CONDOR_DIR_INPUT/" + os.path.basename(self.my_opts_filename)

    wrapfile = open( self.wrapper, "w" )
    wrapfile.write( self.getWrapperHeader() )
    if not self.opts["no_condor"]:
      wrapfile.write( self.getWrapperTarball(tbname) )
    wrapfile.write( self.getWrapperExecute() )

    if self.opts["make_meta"]:
      wrapfile.write( self.getSamHeader() )
      grid = "grid"
      if self.opts["no_condor"]:
        grid = "nogrid"
      nevents = self.opts["n_events"]
      if nevents == -1: #SAM can't handle -1
        nevents = 0
      samrun = "%d%04d" % (self.opts["run"], self.subrun)

      #Reco pool metadata
      metadir = self.reco_meta_grid
      outfile = self.getCondorFileName( self.getPoolOutput(), "RECO" )
      if self.opts["no_condor"]:
        metadir = self.reco_meta
        outfile = self.getPoolOutput()
      wrapfile.write( "sampy $metascript pool reco %s %s %s %s $start_time $stop_time %s %d %s %s %s %s  %s\n" % (self.executable, self.executableArgs, self.cal_input, outfile, metadir, nevents, samrun, self.opts["evt_type"], grid,  self.opts["genie_var"],self.opts["mc_var"]  ) )
      #Reco hist metadata
      metadir = self.hist_meta_grid
      outfile = self.getCondorFileName( self.getHistOutput(), "HIST" )
      if self.opts["no_condor"]:
        metadir = self.hist_meta
        outfile = self.getHistOutput()
      wrapfile.write( "sampy $metascript hist reco %s %s %s %s $start_time $stop_time %s %d %s %s %s %s %s\n" % (self.executable, self.executableArgs, self.cal_input, outfile, metadir, nevents, samrun, self.opts["evt_type"], grid,  self.opts["genie_var"],self.opts["mc_var"]  ) )
      #Reco DST metadata
      if self.opts["do_dst"]:
        metadir = self.dst_meta_grid
        outfile = self.getCondorFileName( self.getDSTOutput(), "DST" )
        if self.opts["no_condor"]:
          metadir = self.dst_meta
          outfile = self.getDSTOutput()
        wrapfile.write( "sampy $metascript dst reco %s %s %s %s $start_time $stop_time %s %d %s %s %s %s %s\n" % (self.executable, self.executableArgs, self.cal_input, outfile, metadir, nevents, samrun, self.opts["evt_type"], grid,  self.opts["genie_var"],self.opts["mc_var"]  ) )
    wrapfile.write( self.getWrapperFooter() )
    wrapfile.close()
    os.chmod( self.wrapper, 0775 )
    if not self.opts["no_condor"]:
      self.addCondorFile( self.cal_input )
      self.addCondorFile( self.my_opts_filename )
      if not self.opts["no_minos"]:
        self.addCondorFile( self.minos_input )
