#!/bin/env python
import os, sys, re
from datetime import datetime
import subprocess

DEBUG = False

class CondorVerbosity:
  """enum style class to help use jobsub's verbosity levels which control when you get email notification"""
  ALWAYS    = 2     #always send e-mail when job completes
  ON_ERROR  = 1     #send e-mail if job completes with error
  NEVER     = 0     #never send e-mail


class CondorJobBuilder:
  """A helper class to aid in the creation of jobs that may submit to condor.  Handles input files, directory maps, pass environmental variables and any other argument to minerva_jobsub."""

  ####################################################################################
  def __init__(self,\
      jobsub_cmd = "jobsub",\
      jobsub_prefix = "",\
      on_grid     = True,\
      interactive = False,\
      high_prio   = False, \
      prod_prio   = False, \
      online_prio = False, \
      accounting_group = None, \
      concurrency_limits = None, \
      extra_args  = "", \
      no_submit   = False, \
      verbosity   = CondorVerbosity.ON_ERROR, \
      executable  = "", \
      executable_args = "", \
      logfile         = "",\
      sam_dataset = None,\
      sam_project = None,\
      usecat = False,\
      setup_minerva   = True,\
      minerva_release = os.getenv("MINERVA_RELEASE"),\
      my_site_root    = os.getenv("MYSITEROOT"),\
      cmt_config      = os.getenv("CMTCONFIG"), \
      package_root    = None, \
      use_hourly_condor_dirs = True, \
      osversion = None, \
      use_role_calibration = False, \
      use_role_data = False, \
      max_n_retries = 5, \
      memory = "2000MB", \
      lifetime = None, \
      xrootd = False, \
      do_not_log_it = False, \
      make_dag = False \
      ):
    
    self.jobsub_cmd    = jobsub_cmd    #script to submit job to condor

    if jobsub_cmd=="jobsub":
      self.jobsub_cmd="jobsub_submit"
    
    self.jobsub_prefix = jobsub_prefix #arg to -prefix of the jobsub script.  job will be named <prefix>_<timestamp>
    self.on_grid = on_grid         #will the job go to the grid?
    self.interactive = interactive #run the job interactively instead of submitting to condor
    self.do_not_log_it = do_not_log_it #if running interative you can disable logs and just print to the screen
    self.sam_station = "minerva-dcache"
    self.high_prio   = high_prio   #run the job with high priority if you are allowed
    self.prod_prio   = prod_prio   #run the job with production priority if you are allowed
    self.online_prio = online_prio #run the job with special online priority if you are minervacal
    self.accounting_group = accounting_group   #accounting group for the job
    self.concurrency_limits = concurrency_limits #concurrency limits for the job to avoid overwhelming certain resources
    self.extra_args = extra_args   #extra arguments to minerva_jobsub
    self.no_submit = no_submit     # create the cmd file but do not submit the job
    self.verbosity = verbosity     #do we want emails always, on error, or never
    self.executable = executable             #will be used as the default for the executable to run
    self.executable_args = executable_args   #will be used as the default for args to the executable
    self.logfile         = logfile           #default logfile
    self.sam_dataset     = sam_dataset
    self.sam_project     = sam_project
    self.usecat = usecat               #???
    self.setup_minerva = setup_minerva       #do you want to setup the MINERvA environment on the worker node?
    self.minerva_release = minerva_release   #MINERvA software release to setup if setting up minerva (e.g. v8r3)
    self.my_site_root = my_site_root         #root location of MINERvA software installations to use if setting up minerva (e.g. /grid/fermiapp/minerva/software_releases/v8r3)
    self.cmt_config = cmt_config #what cmt configuration was used for the build you will run
    self.package_root = package_root #cmt packages root location of the package whose environment you want to set on the grid (e.g. $PRODUCTIONSCRIPTSROOT)
    self.use_hourly_condor_dirs = use_hourly_condor_dirs #creates new condor tmp/exec directories at the top of every hour
    self.osversion = osversion
    self.use_role_calibration = use_role_calibration
    self.use_role_data = use_role_data
    self.max_n_retries = max_n_retries
    if(memory.find("GB")!=-1):
      new_memory = str(int(float(memory.split("GB")[0])*1000))+"MB"
      print "Setting",new_memory
      self.memory=new_memory
    else:
      print "Setting",memory
      self.memory = memory
    self.lifetime = lifetime
    self.xrootd = xrootd
    self.make_dag = make_dag
    self.condor_files = [] #these files will be transferred to the grid via -f <filename>
    self.condor_dirs = {}  #these directories will be mapped to the grid via -d<DIRNAME> <real dir>
    self.condor_envs = []  #these environmental variables will be transferred to the grid via -e <env>
    self.jobsub_flags = ""

    #if no package_root was given, start walked up directories to look for a cmt/setup.sh file.
    #do this event is setup_minerva is False, because we might decide to setup_minerva later, but don't complain that package_root can't be found now.
    # mydir = os.getenv("PWD")
    # while self.package_root == "Find Me" and self.setup_minerva:
    #   if mydir == "/":
    #     if self.setup_minerva:
    #       raise Exception("You want to setup the minerva software environment in this job, but I can't locate a cmt package whose environment I should use.  Try initializing your MinervaCondorJob with package_root = \"some directory\".")
    #     else:
    #       self.package_root = ""
    #   if os.path.isfile( mydir + "/cmt/setup.sh" ):
    #     self.package_root = mydir
    #   else:
    #     mydir = mydir[0:mydir.rfind('/')]

  ####################################################################################
  def updateJobsubClientFlags(self):
    """Update some of the jobsub_submit flags (others are done in
       getJobsubClientCommand())."""

    self.jobsub_flags = ""

    #setup the standard jobsub flags
    if self.on_grid:
      on_osg = os.getenv("OSG_ON")
      on_osg_only = os.getenv("ONLY_OSG_ON")
      if on_osg == "YES" or on_osg_only == "YES" :
        print "Job is going to be submitted into OSG !!!"
        self.jobsub_flags += " --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC,OFFSITE"
        #The following commented lines are to protect against issues with SL6 requiring the use of Singularity with minimal complications. One could modify such that SL6 jobs fix this issue according to the https://cdcvs.fnal.gov/redmine/projects/fife/wiki/Singularity_jobs. It would be straightforward to go back if these warnings change.
        if on_osg_only == "YES" :
          #self.jobsub_flags += " --site Caltech,Colorado,NotreDame,Michigan,UChicago,Wisconsin"
          self.jobsub_flags += " --site Caltech,Colorado,NotreDame,Michigan,Wisconsin"
        else :
          #self.jobsub_flags += " --site Caltech,Colorado,NotreDame,Michigan,UChicago,Wisconsin,FermiGrid"
          self.jobsub_flags += " --site Caltech,Colorado,NotreDame,Michigan,Wisconsin,FermiGrid"

        N_revision=subprocess.check_output("attr -g revision /cvmfs/minerva.opensciencegrid.org", stderr=subprocess.STDOUT, shell=True).split(":")[1].replace("\n","")
        print "CVFMS Revision Number is %s" % N_revision
        self.jobsub_flags += " --append_condor_requirements='(TARGET.CVMFS_minerva_opensciencegrid_org_REVISION>%s)'" % str(int(N_revision)-1)
      else:
        print "Job is going to be submitted into FermiGrid !!!"
        self.jobsub_flags += " --resource-provides=usage_model=DEDICATED,OPPORTUNISTIC"

      # it should be spelled IFDH_FORCE not FORCE_IFDH so this is probably a no-op right now.
      if "minerva" in os.getenv("USER"):
          if self.use_role_calibration:
            self.jobsub_flags += " --role=Calibration "
          elif self.use_role_data:
            self.jobsub_flags += " --role=Data "
          else:
            self.jobsub_flags += " --role=Production "
#      else:
#        print "Removing --use_gftp for now"
#        self.jobsub_flags += " --use_gftp "

      # FERMIHTC lines added in the interest of auto-releasing jobs that are held and upping their memory and time. Amount of increase depends on whether user is minervapro or not. Units of time are seconds. Units of memory are MB.
      self.jobsub_flags += " --lines '+FERMIHTC_AutoRelease=True' "

      if "minervapro" == os.getenv("USER"):
        self.jobsub_flags += " --lines '+FERMIHTC_GraceMemory=1024' "
        self.jobsub_flags += " --lines '+FERMIHTC_GraceLifetime=14400' "       
        if not self.concurrency_limits:
          self.concurrency_limits = "MNVPROD"
      else :
        self.jobsub_flags += " --lines '+FERMIHTC_GraceMemory=100' "
        self.jobsub_flags += " --lines '+FERMIHTC_GraceLifetime=3600' "        
      if self.use_role_calibration:
        self.jobsub_flags += " --role=Calibration "
      elif self.use_role_data:
        self.jobsub_flags += " --role=Data "
        
      #always specify the minerva version of IFDH
      os.environ["IFDH_CP_MAXRETRIES"] = "0"

      if "IFDH_DEBUG" in os.environ.keys():
        self.jobsub_flags += " -e IFDH_DEBUG "
      if "IFDH_CP_MAXRETRIES" in os.environ.keys():
        self.jobsub_flags += " -e IFDH_CP_MAXRETRIES "
      ###if "IFDH_VERSION" in os.environ.keys():
      ###  self.jobsub_flags += " -e IFDH_VERSION "
      ####always specify the minerva version of IFDHC
      ###if "IFDHC_VERSION" in os.environ.keys():
      ###  self.jobsub_flags += " -e IFDHC_VERSION  "
      #if "MINOS_TSQL_ALT_USER" in os.environ.keys():
      #  self.jobsub_flags += " -e MINOS_TSQL_ALT_USER "

      #setting MINOS DB user account name for Data Cal stage
      if not self.make_dag:
        os.environ["ENV_TSQL_USER"] = "minerva"
        if "ENV_TSQL_USER" in os.environ.keys():
          self.jobsub_flags += " -e ENV_TSQL_USER "


    #add dataset defn if there is one
    if self.sam_dataset:
      self.jobsub_flags  += " --dataset_definition=%s" % self.sam_dataset
    if self.sam_project:
      self.jobsub_flags  += " --project_name=%s" % self.sam_project
      self.jobsub_flags += " -e SAM_STATION"

    if   self.verbosity == CondorVerbosity.ALWAYS:
      self.jobsub_flags += " --mail_always"
    elif self.verbosity == CondorVerbosity.ON_ERROR:
      self.jobsub_flags += " --mail_on_error"
    elif self.verbosity == CondorVerbosity.NEVER:
      self.jobsub_flags += " --mail_never"
    else:
      raise Exception( "No verbosity level defined for int value %d. Acceptable values are 0(ALWAYS), 1(ON_ERROR), 2(NEVER)" )

    if self.osversion: #<NOTE: JOBSUB_LITE TESTING ORIGINAL>
      if self.osversion == "SL6" or self.osversion == "sl6": #<NOTE: JOBSUB_LITE TESTING ORIGINAL>
        self.jobsub_flags += " --append_condor_requirements='(TARGET.HAS_SINGULARITY=?=true)' " #<NOTE: JOBSUB_LITE TESTING ORIGINAL>
        ###self.jobsub_flags += " --lines '+SingularityImage=\\\"/cvmfs/singularity.opensciencegrid.org/fermilab/fnal-wn-sl6:latest\\\"' " #<NOTE: JOBSUB_LITE TESTING ORIGINAL>
        self.jobsub_flags += " --singularity-image=/cvmfs/singularity.opensciencegrid.org/fermilab/fnal-wn-sl6:latest" #<NOTE: JOBSUB_LITE TESTING ADDED>
        ###self.jobsub_flags += " --OS=sl6" #<NOTE: JOBSUB_LITE TESTING ADDED>
      else: #<NOTE: JOBSUB_LITE TESTING ORIGINAL>
        self.jobsub_flags += " --OS=%s" % self.osversion #<NOTE: JOBSUB_LITE TESTING ORIGINAL>

    self.jobsub_flags += " --memory=%s" % self.memory
    self.jobsub_flags += " --disk=30GB" 

    if self.lifetime is not None:
      self.jobsub_flags += " --expected-lifetime=%s" % self.lifetime
    
    if self.no_submit:
      self.jobsub_flags += " -n"
    if self.extra_args:
      self.jobsub_flags += " " + self.extra_args

     
    #select accounting group
    if self.accounting_group:
      self.jobsub_flags += " --group=%s" % self.accounting_group
    else:
      self.jobsub_flags += " --group=minerva"

    #prevent to send the e-mail notification for jobs
    self.jobsub_flags += " --mail_never"

    # TODO: Add concurrency limits and priority flags. Not sure how this does/will work with jobsub_client


  ####################################################################################
  def getJobsubClientCommand(self, executable, exec_args, logfile, njobs):
        #update jobsub flags in case properties have changed
    self.updateJobsubClientFlags()

    cmd = ""
    if self.interactive:#run interactively.  just execute
      print "Running job interactively.  Hit ctrl-c to stop."
      cmd = "%s %s" % ( executable, exec_args)
      if logfile and not self.do_not_log_it:
        cmd += " > %s 2>&1" % logfile
        print "   stdout and sterr piped to %s" % logfile
    else:

      cmd = "%s %s " % ( self.jobsub_cmd, self.jobsub_flags) #setup the basic submit command
      ###if self.jobsub_prefix != "": #<NOTE: JOBSUB_LITE TESTING ORIGINAL> 
      ###  cmd += " --prefix %s" % self.jobsub_prefix #<NOTE: JOBSUB_LITE TESTING ORIGINAL>
      ###cmd += " --need-scope storage.create:/pnfs/minerva/scratch/users/minervapro/" #<NOTE: JOBSUB_LITE TESTING ADDED>
      print "cmd from getJobsubClientCommand = %s" % cmd #<NOTE: JOBSUB_LITE TESTING ADDED>

      if njobs > 1:
        cmd += " -N %d" % njobs

      #you only get a logfile if running one job
      if logfile and njobs <= 1:
          cmd += " -L %s " % logfile #add a logfile if specified

      cmd += " %s %s %s " % (self.getCondorFileString(), self.getCondorDirString(), self.getCondorEnvString() )

      if self.setup_minerva:
        cvmfs_choice = os.getenv("CVMFS")
        print "--------------------- cvmfs_choice = %s --------------" % cvmfs_choice
        print "--------------------- package_root = %s --------------" % self.package_root

        if not self.package_root or cvmfs_choice is "YES":
          print "Job submission with tarball package !!!"
          cmd += "  -r %s -i %s  --cmtconfig %s" % ( self.minerva_release,
                                                     self.my_site_root,
                                                     self.cmt_config ) #minerva environmental setup commands

        else:
          """
          cmd += "  -r %s -i %s --cmtconfig %s" % ( self.minerva_release,
                                                    self.my_site_root,                                              
                                                    self.cmt_config ) #minerva environmental setup commands
                                                    """
          print "Job submission via. /minerva/app/users/cmt area !!!"
          cmd += "  -r %s -i %s -t %s --cmtconfig %s" % ( self.minerva_release,
                                                          self.my_site_root,
                                                          self.package_root,
                                                          self.cmt_config ) #minerva environmental setup commands
        


      # We need the full path to the executable here to give to
      # jobsub, so use "which". If executable is already an absolute
      # path, "which" just returns it, which is what we want
      proc=subprocess.Popen(["which", executable], stdout=subprocess.PIPE)
      (executable_fullpath, which_stderr) = proc.communicate()
      executable_fullpath=executable_fullpath.strip()
      cmd += " file://%s %s" % (executable_fullpath, exec_args )

    return cmd

  ####################################################################################
  def getSubmissionCommand(self, executable = None, exec_args = None, logfile = None, njobs = 0):
    njobs = int(njobs)
    #use the member variable execute for
    if executable is None:
      executable = self.executable
      if executable == "":
        raise Exception("You did not specify an executable.  Either set one in the constructor or supply one when you use MinervaJobBuilder.execute")
    if exec_args is None:
      exec_args = self.executable_args #no args is allowed
    if logfile is None:
      logfile = self.logfile

    return self.getJobsubClientCommand(executable, exec_args, logfile, njobs)

  ####################################################################################
  def getSubmissionCommandForDAGInput(self, executable = None, exec_args = None, logfile = None, njobs = 0):
    '''Get the command to submit this stage, but with the necessary
    modifications to allow it to be a line in a dag input file, which
    turns out to not be quite the same thing. It's sad that we have to
    do this.'''
    
    orig_cmd=self.getSubmissionCommand(executable, exec_args, logfile, njobs)

    # With jobsub_client, we can't have --role=... in the dag input
    # file (the option has to be given to jobsub_submit_dag), so
    # remove it
    new_cmd=re.sub(r"--role=\S+", "", orig_cmd)
    print "new_cmd = %s" % new_cmd #<NOTE: JOBSUB_LITE TESTING ADDED>

    return new_cmd

  ####################################################################################
  def execute( self, executable = None, executable_args = None, logfile = None, njobs = 0 ):
    """Execute this job."""

    cmd = self.getSubmissionCommand(executable, executable_args, logfile, njobs)
    if DEBUG:
      print cmd

    submit_succeeded=True
    # Need retry logic for submission, but not for interactive running
    if self.interactive:
      proc=os.popen( cmd ) #todo, we can do better than this execute command
      for i in proc.readlines():
        print i[:-1]
    else:
      submit_succeeded=False
      n_retries=0
      while (not submit_succeeded) and (n_retries<self.max_n_retries):
        print "Try %d at submitting..." % n_retries
        proc=os.popen( cmd ) #todo, we can do better than this execute command
        for i in proc.readlines():
          print i[:-1]
        # Weird return convention: the value of close() is the exit
        # status of the process, unless the exit status was zero (no
        # error), in which case it's None
        submit_succeeded = proc.close() is None
        n_retries += 1

    if not submit_succeeded: 
      raise Exception("Submission failed after all retries")

  ####################################################################################
  def condorClear(self):
    """Clear the condor files, dirs and envs."""
    del self.condor_files[:]
    self.condor_dirs.clear()
    del self.condor_envs[:]

  ####################################################################################
  def condorClearFiles(self):
    """Clear the condor files."""
    del self.condor_files[:]

  ####################################################################################
  def condorClearDirs(self):
    """Clear the condor directories."""
    self.condor_dirs.clear()

  ####################################################################################
  def condorClearEnvs(self):
    """Clear the condor environmental variables."""
    del self.condor_envs[:]

  ####################################################################################
  def addCondorFile( self, f ):

    if DEBUG:
      print " in addCondorFile ",f
    """Add the file to the list of files to send to the condor if it isn't already in the list."""
    #make sure the file exists right now unless we are not submitting right now
    if not os.path.isfile(f) and not self.no_submit and not self.xrootd:
      raise Exception("Cannot send a file to condor if it does not exist: %s" % f)
    if not f in self.condor_files:
      self.condor_files.append( re.sub('/+','/',f) )

  ####################################################################################
  def addCondorFiles( self, files ):
    """Add many condor files."""
    for f in files:
      self.addCondorFile(f)

  ####################################################################################
  def getCondorFileString( self ):
    """Return the string of -f <filename> for all files in this stage's files."""
    ret = ""
    for f in self.condor_files:
      ##Diagnostics for future xrootd
      print "Will I transfer?",f
      #if(".opts" in f or ".dat" in f or not self.xrootd or "flux" in f or "spline" in f or "sntp" in f):#ALWAYS transfer the options file, minos-stage inputs, and genie inputs
      ###if(".opts" in f or ".dat" in f or not self.xrootd or "flux" in f or "gxspl" in f or "sntp" in f or ".txt" in f or ".tgz" in f):#ALWAYS transfer the options file, minos-stage inputs, and genie inputs #NOTE: <JOBSUB_LITE TEST, ORIGINAL>
      if(".opts" in f or ".dat" in f or not self.xrootd or "flux" in f or "gxspl" in f or "sntp" in f or ".txt" in f):#ALWAYS transfer the options file, minos-stage inputs, and genie inputs #NOTE: <JOBSUB_LITE TEST, ADDED>
        print "Yes"
        if "/minerva/data/" in f:
          sys.exit("ERROR: One or more of your inputs are coming from BlueArc. If you specified a BlueArc input please copy your input files to dCache and try again with that input directory. If you get this message without specifying an indir please mail the software list with your jobsubmission command and the filename you see: %s"%(f))
        if (".opts" in f) : #NOTE: <JOBSUB_LITE TEST, ADDED>
          ret += "-f dropbox://%s " % f #NOTE: <JOBSUB_LITE TEST, ADDED>
          #"--use-pnfs-dropbox" is use when RCDS is in a degraded state
          ###ret += "--use-pnfs-dropbox -f dropbox://%s " % f #NOTE: <JOBSUB_LITE TEST, ADDED>
        else : #NOTE: <JOBSUB_LITE TEST, ADDED>
          ret += "-f %s " % f #NOTE: <JOBSUB_LITE TEST, SHIFTED TO THE RIGHT>
        if DEBUG:
          print "condorutils is asking for ",f
    return ret

  ####################################################################################
  def addCondorDir( self, name, dirname ):
    """Add this directory to the list of directories that condor map to its local machine.  Condor's directory name variables are all caps by convention."""
    if not os.path.isdir(dirname): #todo: this may no longer be a problem since jobsub creates directories before doing a copy
      raise Exception("Cannot add directory to condor mapping if it does not exist:%s" % dirname)
    name  = name.upper()
    dirname = re.sub( '/+', '/', dirname )
    if name in self.condor_dirs.keys():
      if dirname != self.condor_dirs[name]:
        raise Exception("Trying to add condor directory %s mapping to %s, but this already maps to %s." % (name, dirname, self.condor_dirs[name] ) )
    else:
      self.condor_dirs[name] = dirname

  ####################################################################################
  def getCondorDirString( self ):
    """Return the strong of -dDIR <dir> for all directories mapped by condor for this stage."""
    ret = ""
    pnfs_dirs = {}
    blue_dirs = {}
    for name,dirname in self.condor_dirs.iteritems():
      if(dirname.find("/pnfs/")!=-1):
        pnfs_dirs[name] = dirname
      else:
        blue_dirs[name] = dirname

    for name,dirname in pnfs_dirs.iteritems():
      ret += "-d %s %s " % (name, dirname )
    for name,dirname in blue_dirs.iteritems():
      ret += "-d %s %s " % (name, dirname )

    return ret

  ####################################################################################
  def addCondorEnv( self, env ):
    """Pass this environmental variable along to condor with the -e ENV flag."""
    if not os.getenv(env):
      raise Exception("Cannot pass environmental variable to condor if it is not set:%s" % env )
    if env not in self.condor_envs:
      self.condor_envs.append( env )

  ####################################################################################
  def getCondorEnvString( self ):
    """Return the string of -e <env> for all environmental variables to be passed along to condor."""
    ret = ""
    for env in self.condor_envs:
      ret += "-e %s " % env
    return ret

  ####################################################################################
  def getCurrentLogFileName( self ):
    """Return the filename of the logfile.  If running inside a condor job return jobsub's tmp logfile name"""
    if not self.interactive:
      ###return "$_CONDOR_SCRATCH_DIR/tmp_job_log_file" #<NOTE: JOBSUB_LITE TESTING, ORIGINAL>
      ###return "${JSB_TMP}/JOBSUB_LOG_FILE" #<NOTE: JOBSUB_LITE TESTING, ADDED (SUGGESTED BY SCD)>
      return "_joblogfile" #<NOTE: JOBSUB_LITE UPDATED, (SUGGESTED BY SCD)>
    else:
      return self.logfile
