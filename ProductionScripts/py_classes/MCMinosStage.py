#!/bin/env python

from MCBaseStage import MCBaseStage
import os, glob, shutil, re
class MCMinosStage(MCBaseStage):
  """Knows how to run the MINOS stage of MC production"""
  def __init__(self, opts):
    #Base Constructor
    MCBaseStage.__init__(self,opts)

    self.exec_flag = "--minos"
    self.my_tag = ""#none.  minos simulation names the output for us.
    self.my_pool_out = "" #NONE!

    # Forces an OS of SL6 since MINOS code has not been migrated to run on SL7. Remove for testing or once this migration occurs.
    if self.opts["os"] == "SL7" or self.opts["os"] == "sl7":
      self.osversion = "SL6"

    # set CondorJobBuilder properties
    #if not accounting group is specified, then use MINOSMC concurrency limits
    if not self.opts["accounting_group"]:
      self.concurrency_limits = "MINOSDB"
    self.setup_minerva = False #override the default from CondorJobBuilder's constuctor
    
    self.script_template = self.opts["opts_file"]
    if self.opts["opts_file"] == "dummy":
      self.script_template = "%s/minos_scripts/run_minos_sim.sh" % os.getenv("PRODUCTIONSCRIPTSROOT")

      if self.opts["minos_overlay"]:
        if(opts["beam"][:2]=="le"):
          self.script_template = "%s/minos_scripts/run_minos_overlay_sim.sh" % os.getenv("PRODUCTIONSCRIPTSROOT")
        else:
          self.script_template = "%s/minos_scripts/run_minos_overlay_ME_sim.sh" % os.getenv("PRODUCTIONSCRIPTSROOT")
      else:
        if(opts["beam"][:2]=="le"):
          self.script_template = "%s/minos_scripts/run_minos_sim.sh" % os.getenv("PRODUCTIONSCRIPTSROOT")
        else:
          self.script_template = "%s/minos_scripts/run_minos_ME_sim.sh" % os.getenv("PRODUCTIONSCRIPTSROOT")

    if self.opts["minos_overlay"]:
      if(opts["beam"][:2]=="le"):
        #self.executable = "/grid/fermiapp/minos/sim/minerva2/process_minerva_mc.sh"  #see $PRODUCTIONSCRIPTSROOT/minos_scripts/run_minos_sim.sh
        self.executable = "/cvmfs/minos.opensciencegrid.org/minos/sim/minerva2/process_minerva_mc.sh"  #see $PRODUCTIONSCRIPTSROOT/minos_scripts/run_minos_sim.sh
        self.executableArgs = "-i $infile -o $outpath -t $inspillfile --overlay=yes --suppression=0.25e-6 $other"
      else:
        #self.executable = "/grid/fermiapp/minos/sim/minerva6/process_minerva_mc.sh"  #see $PRODUCTIONSCRIPTSROOT/minos_scripts/run_minos_sim.sh
        self.executable = "/cvmfs/minos.opensciencegrid.org/minos/sim/minerva6/process_minerva_mc.sh"  #see $PRODUCTIONSCRIPTSROOT/minos_scripts/run_minos_sim.sh
        self.executableArgs = "-i $infile -o $outpath -t $inspillfile --overlay=yes --suppression=0.25e-6 $other"

    else:
      if(opts["beam"][:2]=="le"):
        #self.executable = "/grid/fermiapp/minos/sim/minerva/process_minerva_mc.sh"  #see $PRODUCTIONSCRIPTSROOT/minos_scripts/run_minos_sim.sh
        self.executable = "/cvmfs/minos.opensciencegrid.org/minos/sim/minerva/process_minerva_mc.sh"  #see $PRODUCTIONSCRIPTSROOT/minos_scripts/run_minos_sim.sh
        self.executableArgs = "-i $infile -o $outpath $other"
      else:
        #self.executable = "/grid/fermiapp/minos/sim/minerva6/process_minerva_mc.sh"  #see $PRODUCTIONSCRIPTSROOT/minos_scripts/run_minos_sim.sh
        self.executable = "/cvmfs/minos.opensciencegrid.org/minos/sim/minerva6/process_minerva_mc.sh"  #see $PRODUCTIONSCRIPTSROOT/minos_scripts/run_minos_sim.sh
        self.executableArgs = "-i $infile -o $outpath --overlay=no $other"

    #the following will be set in prepareSubrun() or prepareScript()
    self.exit_input = ""
    if(self.opts["minos_overlay"]):
      self.overlaid_spills_input=""
    self.outbase = ""

  def prepareSubrun( self, sub, usecat=False ):
    MCBaseStage.prepareSubrun( self, sub )
    if(not self.opts["genie_var"]):
      self.exit_input = "%s/%s_%s_%s.dat" % (self.exit_in, self.prefix, self.exit_tag, self.suffix_in)
    else:
      # extract genie_var from the suffix and then append it with an underscore, not a hyphen!
      hGVar = "-"+self.opts["genie_var"]
      hGVIndex = self.suffix_in.find(hGVar)
      self.suffix_in = self.suffix_in[0:hGVIndex]
      self.suffix_in = self.suffix_in + "_" + self.opts["genie_var"]
      self.exit_input = "%s/%s_%s_%s.dat" % (self.exit_in, self.prefix, self.exit_tag, self.suffix_in)
    if(self.opts["minos_overlay"]):
      self.overlaid_spills_input="%s/%s_overlaid_spill_times_%s.dat" % (self.exit_in, self.prefix, self.suffix_out)
    if self.opts["use_argo"]:
      self.exit_input = "%s/%s_%s_%s.dat" % (self.argo_in, self.prefix, self.argo_tag, self.suffix_in)

  def inputIsGood( self ):
    """Check that the input exit particles file exists."""
    if not os.path.isfile(self.exit_input):
      print "Cannot find exiting particle file for input.  Looked at '%s'." % self.exit_input
      return False
    if self.opts["minos_overlay"]:
      if not os.path.isfile(self.overlaid_spills_input):
        print "Cannot find overlaid spills file for input.  Looked at '%s'." % self.overlaid_spills_input
        return False
    return True
  
  def execute( self ):
    """Implementation of the execute method for MINOS.  Create directories, generate the genie script, then use MCBaseStage to run."""
    if len(self.my_dirs) == 0:
      self.prepareDirs()
    self.prepareExecutable()
    MCBaseStage.execute( self )

  def prepareDirs( self ):
    """Make the directories that the MINOS stage needs.  Define the directories for condor."""

    self.my_dirs.append( self.scripts_out )
    self.my_dirs.append( self.logs_out )

    self.my_dirs.append( self.minos_out )

    if self.opts["make_meta"]:
      self.my_dirs.append( self.minos_meta )

    self.makeDirs()

    if not self.opts["no_condor"]:   #create the condor directory maps unless running interactively
      self.addCondorDir("MINOS", self.minos_out)
      if self.opts["make_meta"]:
        self.addCondorDir("MINOSMETA", self.minos_meta )

  def prepareExecutable( self, tbname ):
    """Generate the script that runs the MINOS job.  Define executable and executableArgs."""
    self.logfile = "%s/MinosSim_%s_%s.log" % (self.logs_out, self.prefix, self.suffix_out) #set the logfile name
    self.wrapper = "%s/MinosSim-wrapper_%s_%s.sh" % ( self.scripts_out, self.prefix, self.suffix_out )
    print "Wrapper is", self.wrapper
    self.outbase = "%s/%s_%s_%s" % (self.minos_out, self.prefix, self.exit_tag , self.suffix_out )
    if self.opts["use_argo"]:
      self.outbase = re.sub(self.exit_tag, self.argo_tag, self.outbase)
    
    #create the script which will run this job
    template = open( self.script_template, "r")
    wrapfile = open( self.wrapper, "w" )
    wrapfile.write( self.getWrapperHeader() )

    #if not self.opts["no_condor"] and not self.opts["bluearc"]:
    if not self.opts["no_condor"]:
      wrapfile.write( self.getWrapperTarball(tbname) )

    for line in template:
      newline = line
      if re.search(r'SCRIPTPATH', newline ):
        continue #we want to write the execute lines ourselves
      if self.opts["no_condor"]:
        newline = re.sub(r'^infile=.*',r'infile=%s\n' % self.exit_input, newline)
        if self.opts["minos_overlay"]:
          newline = re.sub(r'^inspillfile=.*',r'inspillfile=%s\n' % self.overlaid_spills_input, newline)
        newline = re.sub(r'scratch=.*',r'scratch=%s\n' % self.minos_out, newline)
        newline = re.sub(r'outpath=.*',r'outpath=%s\n' % self.minos_out, newline)
      else:
        newline = re.sub(r'^infile=.*',r'infile=%s\n' % self.getCondorFileName(self.exit_input), newline)
        if self.opts["minos_overlay"]:
          newline = re.sub(r'^inspillfile=.*',r'inspillfile=%s\n' % self.getCondorFileName(self.overlaid_spills_input), newline)
        newline = re.sub(r'outpath=.*',r'outpath=%s\n' % self.minos_grid, newline)
      newline = re.sub(r'^run=.*',r'run=%d\n' % self.opts["run"], newline)
      newline = re.sub(r'^subrun=.*',r'subrun=%d\n' % self.subrun, newline)
      newline = re.sub(r'nevents=.*',r'nevents=%d\n' % self.opts["n_events"], newline)
      if self.opts["n_events"] > 0: #if nevents is specified, then remove the comment which blocks the use of nevents
        newline = re.sub(r'^#BLOCK_NEVENTS-','', newline)
      newline = re.sub(r'bfield=.*',r'bfield=%d\n' % self.opts["bfield"], newline )
      wrapfile.write( newline )

    wrapfile.write( self.getWrapperExecute() )
    
    #move the output files if output tag is different from input tag
    #minos names files for us as   basename( exit_particles_more_stuff.dat ) + <extension>
    if self.opts["outtag"] != self.opts["intag"]:
      inbase = os.path.join(self.minos_out, os.path.basename( self.exit_input ).replace(".dat","") )
      wrapfile.write("echo moving MINOS output files to change file tag from '%s' to '%s'\n" % ( self.opts["intag"], self.opts["outtag"] ) )
      wrapfile.write("mv %s.detsim.root %s.detsim.root\n" % (inbase, self.outbase ) )
      wrapfile.write("mv %s.detsim.log %s.detsim.log\n" %   (inbase, self.outbase ) )
      wrapfile.write("mv %s.sntp.root %s.sntp.root\n" %     (inbase, self.outbase ) )
      wrapfile.write("mv %s.reco.log %s.reco.log\n" %       (inbase, self.outbase ) )

    self.sntp_output = "%s.sntp.root" % (self.outbase) #note already corrected for minos_out vs grid_minos_out directories
    print "old sntp out ",self.sntp_output
    self.sntp_output = self.minos_grid+"/"+os.path.basename(self.sntp_output) #os.path.join(os.getenv("CONDOR_DIR_MINOS"),os.path.basename(self.sntp_output))
    print "new sntp out ",self.sntp_output
    if self.opts["make_meta"]:
      wrapfile.write( self.getSamHeader() )
      grid = "grid"
      if self.opts["no_condor"]:
        grid = "nogrid"
      nevents = self.opts["n_events"]
      if nevents == -1: #SAM can't handle -1
        nevents = 0
      samrun = "%d%04d" % (self.opts["run"], self.subrun)

      metadir = self.minos_meta_grid
      if self.opts["no_condor"]:
        metadir = self.minos_meta

      if not self.opts["no_condor"]:
        exec_string = """

echo source %s/cmt/setup.sh for PRODUCTIONSCRIPTS
pushd %s/cmt/
pwd
cmt config
source setup.sh
popd
pwd
""" % (os.getenv("PRODUCTIONSCRIPTSROOT"), os.getenv("PRODUCTIONSCRIPTSROOT"))
        wrapfile.write( exec_string ) 

      #Minos sntp metadata
      wrapfile.write( "export PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH; python $PRODUCTIONSCRIPTSROOT/mc_scripts/MakeMCJson.py  sntp minos %s %s %s %s $start_time $stop_time %s %d %s %s %s %s %s\n" % (self.executable, "none", self.exit_input, self.sntp_output, metadir, nevents, samrun, self.opts["evt_type"], grid,  self.opts["genie_var"],self.opts["mc_var"]  ) )
      
    wrapfile.write( self.getWrapperFooter() )
    wrapfile.close()
    os.chmod( self.wrapper, 0775 )

    self.addCondorFile( self.exit_input )
    if self.opts["minos_overlay"]:
      self.addCondorFile( self.overlaid_spills_input )
