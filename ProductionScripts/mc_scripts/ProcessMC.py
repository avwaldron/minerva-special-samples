#!/bin/env python

from MCGenieStage import MCGenieStage
from MCCalStage import MCCalStage
from MCCalRockStage import MCCalRockStage
from MCCalPCStage import MCCalPCStage
from MCCalPCExtStage import MCCalPCExtStage
from MCMuonStage import MCMuonStage
from MCArgoStage import MCArgoStage
from MCMinosStage import MCMinosStage
from MCRecoStage import MCRecoStage
from MCAnaStage import MCAnaStage

from MCDSTStage import MCDSTStage

from MCMEUStage import MCMEUStage

from MinervaProductionSoftwareMap import MinervaProductionMap

import glob, sys, os, time, subprocess, optparse, socket
from datetime import datetime
from re import match
import re
if(os.getenv("MINERVA_RELEASE").startswith("v10") or os.getenv("MINERVA_RELEASE").startswith("v20")):
  print "This is a pre-Inexstinguishable version. Loading OLD MCStandardRun"
  from MCStandardRun import MCStandardRun
else:
  print "This is an Inexstinguishable version. Loading Latest MCStandardRun"
  from MCStandardRunExpanded import MCStandardRun

   



##########################################
#
# MCOptionParser defines all the options
#  for the ProcessMC script.
#
##########################################
class MCOptionParser( optparse.OptionParser ):
  """This class inherits from OptionParser.  It adds functionality specific to processing MINERvA MC.  It knows all the options which you are allowed to give to ProcessMC.  The information parsed by class is passed to each MC Stage, and it is assumed that the Stage knows what to do with it.  Note that the defaults added to the parser are not necessarily the real defaults but rather placeholders to tell the parser whether you supplied that option."""
  def __init__(self):
    formatter = optparse.TitledHelpFormatter( max_help_position=22, width = 190 )
    optparse.OptionParser.__init__(self, formatter = formatter ) #become an OptionParser

  #Add your special error handling here
  #def error(self, msg):
  #  pass

  def showHelp( self ):
    self.parse_args("--help".split())

  def addOpts_All( self ):
    #add all the options that MINERvA MC wants
    self.addOpts_Common( )
    self.addOpts_WhatToRun( )
    self.addOpts_IO( )
    self.addOpts_JobSub( )
    self.addOpts_Genie( )
    self.addOpts_Flux( )
    self.addOpts_AF( )
    self.addOpts_Minos()
    self.addOpts_Help()

  def addOpts_Common( self ):
    comm = optparse.OptionGroup( self, "Common Options" )
    comm.add_option("-r","--run", dest="run", default="-1", type="int", help="Run Number (required)")
    comm.add_option("-d", "--det", dest="det", default="minerva", help="Detector. ( [minerva], frozen )")
    comm.add_option("-f","--first_subrun", dest="first_subrun", default=-1, type="int", help="First subrun [1]")
    comm.add_option("-l","--last_subrun", dest="last_subrun", default=-1, type="int", help="Last subrun [1]")
    comm.add_option("-s", "--subrun", "--subruns", dest="subruns", default="dummy", help="List of subruns to process. Can contain a comma-separated list and/or range. Eg, 1,4-10,11  This option will override the first/last subrun arguments.")
    comm.add_option("--exclude_subs",dest="exclude_subs", default="dummy" , help="Exlude this comma-separated list of subruns.")
    comm.add_option("-n","--n_events", dest="n_events", default=-1, type="int", help="Number of events (default is 100 for GENIE/ParticleCannon, all events for all else)")
    comm.add_option("--ignore_warnings",dest="ignore_warnings", default=False, action="store_true", help="Don't pay attention to warnings given by these scripts. (for experts only)")
    comm.add_option("--genie_var", "--genie_variation", dest="genie_var", default="dummy", help="GENIE Variation used during the production of the GENIE stage and naming thereafter.  use \"--help-genie_var\" for more info.")
    comm.add_option("--mc_var", "--mc_variation", dest="mc_var", default="dummy", help="MC Variation used during the production of the Cal stage and naming thereafter.  use \"--help-mc_var\" for more info.")
    comm.add_option("--playlist", dest="playlist", default="dummy", help="Filename of the playlist that contains data cal digits for DataMC overlay use. (Default is encoded by standard runs or taken base on beam selection)")
    comm.add_option("--n_data_files", dest="n_data_files", default=900, type="int", help="Number of data files needed for data overlay (default chosen based on playlist)")
    comm.add_option("--test", dest="test", default=False, action="store_true", help="Print a summary of what will happen but don't execute anything")
    comm.add_option("--kill_switch", dest="kill_switch", default=True, help="Production Scripts will not run if kill_switch is not explicitly set to False")
    self.add_option_group(comm)

  def addOpts_WhatToRun( self ):
    stages = optparse.OptionGroup( self, "What to Run" )
    stages.add_option("--genie", dest="do_genie", action="store_true", default=False, help="Run GENIE to generate neutrino events in MINERvA")
    stages.add_option("--cal", dest="do_cal", action="store_true", default=False, help="Run Geant4 and Readout from GENIE input")
    stages.add_option("--cal-rock", dest="do_rock", action="store_true", default=False, help="Run Geant4 and Readout from Rock Muon MC")
    stages.add_option("--cal-pc", dest="do_pc", action="store_true", default=False, help="Run Geant4 and Readout from Particle Cannon")
    stages.add_option("--cal-pc-ext", dest="do_pc_ext", action="store_true", default=False, help="Run Geant4 and Readout from Particle Cannon, external particle input")
    stages.add_option("--mc-muon", dest="do_mc_muon", action="store_true", default=False, help="Run MC Rock Muons for S2S and Timing Calibration")
    stages.add_option("--argo", dest="do_argo", action="store_true", default=False, help="Run ArgoNeuT Simulation")
    stages.add_option("--minos", dest="do_minos", action="store_true", default=False, help="Run MINOS Simulation")
    stages.add_option("--reco", dest="do_reco", action="store_true", default=False, help="Run Reconstruction")
    stages.add_option("--ana", dest="do_ana", action="store_true", default=False, help="Run Analysis")
    stages.add_option("--meu", dest="do_meu", action="store_true", default=False, help="Run Reco for MEU Tuning using a rock muon playlist file ")
    stages.add_option("--dst", dest="do_dst", action="store_true", default=False, help="Use DSTWriter to produce a DST output.  Is its own stage when [ no other action is desired OR running DAG with a POOL output]")
    self.add_option_group(stages)

  def addOpts_IO( self ):
    io = optparse.OptionGroup( self, "Input / Output" )
    defdir = "/pnfs/minerva/scratch/users/" + os.getenv("USER") + "/mc_production"
    io.add_option("--usecat", dest="usecat", action="store_true",default=True, help="use sam catalog to find the file")
    io.add_option("--nocat",dest="usecat", action="store_false",default=False,help="don't use sam catalog to find the input file")
    io.add_option("--indir", dest="indir", default="dummy", help="Top-level input directory (default = /minerva/data/genie for genie input or " + defdir +" for all else)")
    io.add_option("--outdir", dest="outdir", default=defdir, help="Top-level output directory (default = " + defdir +")")
    io.add_option("--intag", dest="intag", default="", help="Filetag for input files goes after vXrY (default = \"\")")
    io.add_option("--outtag", dest="outtag", default="", help="Filetag for output files goes after vXrY (default = \"\")")
    io.add_option("--inv", dest="inv", default = os.getenv("MINERVA_RELEASE"), help="AF version used to create input files (default = " + os.getenv("MINERVA_RELEASE") + ")" )
    io.add_option("--genie_v", dest="genie_v", default = os.getenv("MNVGENIE_VERSION"), help="What MINERVA-GENIE version are you using. (default = " + os.getenv("MNVGENIE_VERSION") + ")")
    io.add_option("--ingrid", dest="ingrid", action="store_true", default = "", help="Input was processed on the grid (default if you are using grid now)")
    io.add_option("--innogrid", dest="innogrid", action="store_true", default = "", help="Input was NOT processed on the grid (default if you are not using grid now)")
    io.add_option("--outgrid", dest="outgrid", action="store_true", default = False, help="Force output to the \"grid\" directories (experts only)")
    io.add_option("--no_vardir", dest="no_vardir", action="store_true", default = False, help="Do not put the mc_var string (or central_value) in the directory structure.")
    io.add_option("--rockdir", "--rockmuonsdir", dest="rockmuonsdir", default = "", help="Directory that contains files containing rock muon playlists with names MEU_RockMuons_XXXX.txt")
    io.add_option("--ext_particle_dir", dest="ext_particle_dir", default="", help="Directory that contains external particles files")
    io.add_option("--multiple_ext_particles", dest="multiple_ext_particles", action="store_true", default=False, help="Generate multiple particles per interaction when used with --cal-pc-ext")
    io.add_option("--production_ops", dest="production_ops",action="store_true", default=False, help="Flag used by OPOS group members to submit minervapro jobs from their individual accounts")
    io.add_option("--override_dag_location", dest="dag_loc",default="",help="Override default dag output location which is found in MCBaseStage.py")
    io.add_option("--tarball", dest="tarball", default="dummy", help="Tarball name and location to run")
    #io.add_option("--bluearc", dest="bluearc", action="store_true", default=False, help="Turn on BlueArc area access, set False for BlueArc unmounting case")
    io.add_option("--resilient_dir", dest="resilient_dir", default="/pnfs/minerva/resilient/tarballs/", help="Official resilient directory")
    self.add_option_group(io)

  def addOpts_JobSub( self ):
    jobopts = optparse.OptionGroup( self, "Job Submission" )
    jobopts.add_option("--prefix", dest="job_prefix", default="", help="Chose a prefix for the wrapper file (condor cmd name).  Wrapper will be named <prefix>_Stage_run_subrun_det.")
    jobopts.add_option("-a","--grid_args", dest="grid_args", default="", help="Additional args to jobsub_submit (the args must be enclosed in quotes)")
    jobopts.add_option("--max", "--max_jobs", dest="max_jobs", default = 200, type="int", help="(experts only.  you probably don't want this).  Maximum number of concurrent jobs allowed to run within a DAG.  Set to -1 for no limit.  (default = 200)")
    jobopts.add_option("--no_dag", dest="make_dag", default=True, action="store_false", help="Submit my jobs one by one without using a DAG (not recommended)")
    jobopts.add_option("--no_submit", dest="no_submit", action="store_true", default=False, help="Create the cmd file to be submitted to condor later.  Do not check for input files.")
    jobopts.add_option("--no_grid", dest="no_grid", action="store_true", default=False, help="Run on if machine condor pool instead of grid (not recommended)")
    jobopts.add_option("--interactive","--no_condor", dest="no_condor", action="store_true", default=False, help="Run interactively on this machine (not allowed on if01)")
    jobopts.add_option("--high_prio", dest="high_prio", action="store_true", default=False, help="Submit condor jobs with special high priority (only if you are authorized)")
    jobopts.add_option("--prod_prio", dest="prod_prio", action="store_true", default=False, help="Submit condor jobs with special production priority (only if you are authorized)")
    jobopts.add_option("--accounting_group", "--Agroup", dest="accounting_group", default=None, help="Submit condor jobs with this accounting group.")
    jobopts.add_option("--condor_verbosity", dest="condor_verbosity", type="int", default=1, help="When should condor send an email upon job completion? (0=never, 1=on error, 2=always) (default=1)")
    jobopts.add_option("--pause", dest="pause", default=1, type="int", help = "Delay between job submissions (default is 1s)")
    jobopts.add_option("--make_meta", dest="make_meta", action="store_true", default=False, help="Make SAM metadata file")
    jobopts.add_option("--reproduce_meta", dest="reproduce_meta", action="store_true", default=False, help="Make SAM metadata file only.  do no processing and don't overwrite any existing files (except metadata).  Assumes processing is already done.")
    jobopts.add_option("--use_sl4", dest="use_sl4", action="store_true", default=False, help="Run using an sl4 32-bit build (default is current \$PLATFORM)")
    jobopts.add_option("--use_sl5", dest="use_sl5", action="store_true", default=False, help="Run using an sl5 64-bit build (default is current \$PLATFORM)")
    jobopts.add_option("--os", dest="os", default="SL6", help="Run on a grid machine with given OS version: see --OS option of jobsub. Default is SL5")
    jobopts.add_option("--sl6_compat", dest="sl6compat", action="store_true", default=False, help="Submit job under sl6 compatibility mode")
    jobopts.add_option("--minos_sl7_override", dest="minos_SL7_override", action="store_true", default=False, help="Agree to risks associated with running MINOS with SL7 specified. MINOS jobs currently will always run an SL6 Singularity job, regardless of what other stages run. Unique to MINOS Simulation.")
    jobopts.add_option("--use_role_calibration", dest="use_role_calibration", action="store_true", default=False, help="Submit job using role of Calibration.")
    jobopts.add_option("--use_role_data", dest="use_role_data", action="store_true", default=False, help="Submit job using role of Data.")
    jobopts.add_option("--memory",dest="memory", default="2000MB", help="Request a different amount of memory. See jobsub_submit --help for format")
    jobopts.add_option("--xrootd",dest="xrootd", action="store_true", default=True, help="Request to use xrootd. On by default. See jobsub_submit --help for format")
    jobopts.add_option("--lifetime",dest="lifetime", default=None, help="Request a different job lifetime, using the --expected-lifetime option of jobsub_submit. See jobsub_submit --help for format")
    jobopts.add_option("--osg", dest="osg", action="store_true", default=False, help="Request to run the job in OSG, Off by default See jobsub_submit --help for format")
    jobopts.add_option("--osg_only", dest="osg_only", action="store_true", default=False, help="Request to run the job in OSG only (without FermiGrid), Off by default See jobsub_submit --help for format")

    self.add_option_group(jobopts)


  def addOpts_Flux( self ):
    fluxopts = optparse.OptionGroup(self, "Flux options")
    fluxopts.add_option("--xCenter", dest="flux_xCenter", default="dummy", help="X-Center of flux in mm (default is what's in options template)")
    fluxopts.add_option("--yCenter", dest="flux_yCenter", default="dummy", help="Y-Center of flux in mm (default is what's in options template)")
    fluxopts.add_option("--zCenter", dest="flux_zCenter", default="dummy", help="Z-Center of flux in mm (default is what's in options template)")
    fluxopts.add_option("--deltaZ", dest="flux_deltaZ", default="dummy", help="Allowed deviation from Z-Center for flux in mm (default is what's in options template.  If zCenter is specified the default becomes 0mm.)")
    fluxopts.add_option("--radius", dest="flux_radius", default="dummy", help="Allowed radius for flux in mm (default is what's in options template)")
    fluxopts.add_option("--time", dest="flux_time", default="dummy", help="Vertex time in ns (default is what's in options template)")
    fluxopts.add_option("--particle", dest="pc_particles", type="string", action="append", help="Definition of a ParticleCannon particle (must be enclosed in quotes).  You can use --particle as many times as you would like.  Enter --help-pc for further help.")
    fluxopts.add_option("--vertex", dest="pc_vertices", type="string", action="append", help="Definition of a ParticleCannon vertex (must be enclosed in quotes).  You can use --vertex as many times as you would like.  Enter --help-pc for further help.")
    self.add_option_group(fluxopts)

  def addOpts_Genie( self ):
    genieopts = optparse.OptionGroup( self, "GENIE Options" )

    genieopts.add_option("--pot", dest="pot", default="dummy", help="Protons on target exposure for each subrun.  Not compatible with flux Histos. (default is to use nevents)")
    genieopts.add_option("--geo", dest="geo", default="dummy", help="Generate events in a standard ROOT geometry variant ( [ID_NoCal], NukeRegion, DSCal, OD, Cryo, Full )")
    genieopts.add_option("--geo_file", dest="geo_file", default="dummy", help="Generate events in this ROOT geometry file (default is to use standard geometries)")
    genieopts.add_option("--target_list", dest="target_list", default=None, help="Generate events in this list of targets.  format is code1[frac1],code2[frac2],... (default is to use standard geometries)")
    genieopts.add_option("--beam", dest="beam", default="dummy", help="Beam configuration.  For ntuples, use le010z185i for MINERvA default and le010z-185i for Frozen default. ( [le010z185i], le010z-185i, le100z200i,... ).  For histos, use LE for default ([LE], ME)")
    genieopts.add_option("--evt", "--evt_type", dest="evt_type", default="mixed", help="Event type to generate ( [mixed] You can find a full list of options at $GENIEROOT/x86_64-slc6-gcc44-opt/GENIE/config/EventGeneratorListAssembler.xml )")
    genieopts.add_option("--ntp_files", dest="ntp_files", default="dummy", help="Use these NTuples instead of standard ones.  Enclose in quotes if using globbing format. (not recommended)")
    genieopts.add_option("--histo_file", dest="histo_file", default="dummy", help="Use this histo flux file instead of standard one.  (not recommended)")
    genieopts.add_option("--histos", dest="use_histos", action="store_true", default=False, help="Use NuMI flux histograms instead of ntuples (not recommended)")
    genieopts.add_option("--nu_flux", dest="nu_flux",  default="dummy", help="Comma-separated list of neutrino you want to include from the NuMI flux histograms instead of ntuples.  e.g. numu,numubar (deafult is all)")
    genieopts.add_option("--flux_dir", dest="flux_dir",  default="dummy", help="Base directory for flux files")
    genieopts.add_option("--gxml_dir", dest="gxml_dir",  default="/grid/fermiapp/minerva/genie_xsec/v2_12_6/NULL/DefaultPlusValenciaMEC/data/", help="Location of the configuration xml files")
    
    self.add_option_group(genieopts)

  def addOpts_AF( self ):
    """Add options relevant only to AnalysisFramework jobs (i.e. jobs that use an options file)."""
    AFopts = optparse.OptionGroup(self, "Analysis Framework Options")
    AFopts.add_option("--opts", dest="opts_file", default="dummy", help="Use a special options template file.  For single action jobs only.  (default is to use SystemTests/options)")
    AFopts.add_option("--pf","--print_freq", dest="print_freq", default=100, type="int", help="Print frequency for Gaudi jobs. (default is 100)")
    AFopts.add_option("--first_event",dest="first_event", default=1, type="int", help="EventSelector's FirstEvent  (default is 1)")
    AFopts.add_option("--dst_fCal", dest="dst_fCal", default=False, action="store_true", help="Use cal POOL file as input for DSTWriter.  For DST stage only. (default is to use reco POOL as input)")
    AFopts.add_option("--gfilter", dest="gfilter", default="dummy", help="GFilter options file to be included, which will filter GENIE input events.  For Cal only.")
    AFopts.add_option("--no_weights", dest="no_weights", default=False, action="store_true", help="Do not create GENIEWeights during Generation.  For Cal only. (default is to make weights)")
    AFopts.add_option("--singlenu", dest="singlenu", default=False, action="store_true", help="Generate one neutrino interaction per spill.  For Cal only. (default is singlenu for user-defined runs)")
    AFopts.add_option("--multinu", dest="multinu", default=False, action="store_true", help="Generate multiple neutrino interactions per spill \"realistically\".  For Cal only.")
    AFopts.add_option("--overlay", dest="overlay", default=False, action="store_true", help="Overlay MC onto Data gates in Generation.  For Cal only.")
    AFopts.add_option("--force_minerva_overlay_off", dest="force_minerva_overlay_off", action="store_true", default=False, help="Force minerva overlay off no matter what software version or MC configuration")
    AFopts.add_option("--xtalk", "--xtalk_scale", dest="xtalk_scale", default=1.5, type="float", help="Set the global X-talk scale factor.  For Cal only. (default is 1.5)")
    AFopts.add_option("--mi_state",  dest="mi_state", default=14, type="int", help="Main Injector spill time structure.  For Cal only. (8, [14], 21)")
    AFopts.add_option("--no_flux_rec", dest="no_flux_rec", default=False, action="store_true", help="Don't require that the GENIE files contain a flux record.  For Cal only. (default is False for ntuple flux, True for histos flux)")
    AFopts.add_option("--no_minos", dest="no_minos", default=False, action="store_true", help="Don't use MINOS as input.  Don't run MinosDataAlg.  For Reco only. (default is require MINOS)")
    AFopts.add_option("--with_water", dest="with_water", default=False, action="store_true", help="Include the Water Target in the geometry description for geant simulation.")
    AFopts.add_option("--with_cryo", dest="with_cryo", default=False, action="store_true", help="Include the Cryo Target in the geometry description for geant simulation.")
    AFopts.add_option("--with_empty_cryo", dest="with_empty_cryo", default=False, action="store_true", help="Include the Cryo Target in the geometry description for geant simulation.")
    AFopts.add_option("--with_veto", dest="with_veto", default=False, action="store_true", help="Include the VetoDetector in the geometry description for geant simulation.")
    AFopts.add_option("--ana_tool", "--anatool", dest="ana_tool", default=None, help="NOTICE: If you are running a minerva analsysis tool then use ana_scripts/ProcessAna.py.")

    self.add_option_group(AFopts)

  def addOpts_Minos( self ):
    """Add options relevant to only MINOS jobs."""
    extOpts = optparse.OptionGroup(self,"MINOS Options")
    extOpts.add_option("--bfield", dest="bfield", default = "dummy", help="Polarity of MINOS coil. 1(-1) for forward mu-(reverse mu+) coil current.  Default is forward for positive or zero horn current and reverse for negative horn current.")
    extOpts.add_option("--no_argo", dest="use_argo", default=True, action="store_false", help="Do NOT use the output from the Argoneut simulation.  Relevant only for frozen detector MINOS stage.")
    extOpts.add_option("--minos_overlay", dest="minos_overlay", action="store_true", default=False, help="Run MINOS Simulation with data overlay")
    extOpts.add_option("--force_minos_overlay_off", dest="force_minos_overlay_off", action="store_true", default=False, help="Force minos overlay off no matter what software version or MC configuration")
    self.add_option_group(extOpts)

  def addOpts_Help( self ):
    """Add options relevant for getting help."""
    helpOpts = optparse.OptionGroup(self,"Help Menu")
    helpOpts.add_option("--help-action", dest="help_action", action="store_true", default=False, help="See help related to Actions (what you can run)")
    helpOpts.add_option("--help-common", dest="help_common", action="store_true", default=False, help="See help related to Common options (e.g. detector choice, statistics)")
    helpOpts.add_option("--help-jobsub", dest="help_jobsub", action="store_true", default=False, help="See help related to Job Submission")
    helpOpts.add_option("--help-io", dest="help_io", action="store_true", default=False, help="See help related to Input/Output")
    helpOpts.add_option("--help-genie", dest="help_genie", action="store_true", default=False, help="See help related to GENIE")
    helpOpts.add_option("--help-pc", dest="help_pc", action="store_true", default=False, help="See help related to Particle Cannon")
    helpOpts.add_option("--help-AF", "--help-af", dest="help_af", action="store_true", default=False, help="See help related to analysis framework jobs.")
    helpOpts.add_option("--help-minos", dest="help_minos", action="store_true", default=False, help="See help related to MINOS Simulation.")
    helpOpts.add_option("--help-genie_var", dest="help_genie_var", action="store_true", default=False, help="See help related to GENIE variations, including a list of known variations.")
    helpOpts.add_option("--help-mc_var", dest="help_mc_var", action="store_true", default=False, help="See help related to MC variations, including a list of known variations.")
    self.add_option_group(helpOpts)

  def stringToIntList(self, s ):
    '''Convert a string of comma-separated numbers into a list, where the
       string may also contain ranges like "N-M". So some valid strings are
       "1,2", "1,3,10-20,21" etc'''
    ret=[]
    try:
      for i in s.split(","):
        if "-" in i:
          tmp=i.split("-")
          ret+=range(int(tmp[0]), int(tmp[1])+1)
        else:
          ret.append(int(i))
    except ValueError:
      raise Exception("stringToIntList: invalid subrun range string \"%s\"" % s)
    return ret
    
  ##################################
  #Apply the real defaults
  ##################################
  def applyDefaults( self, opts ):
    if opts["run"] == -1:
      raise Exception("You must supply a run number.  Use like --run <run>.")

    #turn the subruns arg into a list
    if opts["subruns"] != "dummy":
      opts["subruns"] = self.stringToIntList(opts["subruns"])


    #if neither subrun is set, use subrun 1 for both
    if opts["first_subrun"] == -1 and opts["last_subrun"] == -1:
      opts["first_subrun"] = 1
      opts["last_subrun"]  = 1

    #if only one subrun is set, set the other to the same value
    if opts["first_subrun"] == -1 and opts["last_subrun"] != -1:
      opts["first_subrun"] = opts["last_subrun"]
    if opts["last_subrun"] == -1 and opts["first_subrun"] != -1:
      opts["last_subrun"] = opts["first_subrun"]

    #if first->last subrun used, then put that range into the subruns array
    if opts["subruns"] == "dummy":
      opts["subruns"] = range(opts["first_subrun"], opts["last_subrun"]+1)

    #exmaple exclude subs into a tuple
    if opts["exclude_subs"] != "dummy":
      opts["exclude_subs"] = [ int(x) for x in opts["exclude_subs"].split(",") ]

    #indir default
    if opts["indir"] == "dummy":
      opts["indir"] = "/minerva/data/users/" + os.getenv("USER") + "/mc_production"
      if opts["do_cal"] and opts["run"] > 999:
        opts["indir"] = "/minerva/data/genie/"

    #conform to storing detector string
    opts["det"] = opts["det"].lower()
    if opts["det"] == "downstream":
      opts["det"] = "frozen"
    if opts["det"] not in ["minerva","frozen"]:
      raise Exception("Detector %s is not defined.  Use 'minerva' or 'frozen'." % opts["det"])

    #if no geometry type is given, use ID_NoCal (only affects genie)
    if opts["geo"] == "dummy" and not opts["target_list"]:
      opts["geo"] = "ID_NoCal"

    if opts["geo"] != "dummy" and opts["target_list"]:
      raise Exception("You cannot specify both a ROOT geometry and a list of targets for event generation.")


    #cryo geometry only exists for MINERVA detector
    if opts["geo"] == "Cryo" and not opts["det"] == "minerva":
      raise Exception("There is no geometry Cryo for the %s detector.  It only exists for MINERVA." % opts["det"])

    #water, cryo and veto selections only make sense with the MINERvA detector
    if opts["with_water"] and not opts["det"] == "minerva":
      raise Exception("Water Target cannot be used for detector %s.  Use of Water Target only makes sense with the minerva detector." % opts["det"])

    if opts["with_cryo"] and not opts["det"] == "minerva":
      raise Exception("CryoTarget cannot be used for detector %s.  Use of CryoTarget only makes sense with the minerva detector." % opts["det"])

    if opts["with_empty_cryo"] and not opts["det"] == "minerva":
      raise Exception("CryoTarget cannot be used for detector %s.  Use of CryoTarget only makes sense with the minerva detector." % opts["det"])

    if opts["with_veto"] and not opts["det"] == "minerva":
      raise Exception("VetoDetector cannot be used for detector %s.  Use of VetoDetector only makes sense with the minerva detector." % opts["det"])

    #if no beam is specified, use le010z185i, except for frozen then use le010z-185i
    if opts["use_histos"]:
      if opts["beam"] == "dummy":
        opts["beam"] = "LE"
      if opts["beam"] not in ["LE","ME"]:
        raise Exception("Beam %s is not defined for histos.  Choices are LE or ME" % opts["beam"] )
    else:
      if opts["beam"] == "dummy":
        if opts["det"] == "frozen":
          opts["beam"] = "le010z-185i"
        else:
          opts["beam"] = "le010z185i"

      if not match(r"[ml]e\d{3}z-?\d{3}i", opts["beam"]):
          raise Exception("Beam %s is no defined.  The format is like leXXXzYYYi or meXXXz-YYYi." % opts["beam"] )

    #select default playlist to mimic based on beam

    releaseName = getReleaseName()
    print "THIS IS MY RELEASE",releaseName, opts["playlist"], opts["beam"]
    if opts["playlist"] == "dummy":
      print "RELEASENAME : ", releaseName, os.getenv("PRODUCTIONSCRIPTSROOT")
      playlistTopDir = "%s/mc_scripts/playlists" % ( os.getenv("PRODUCTIONSCRIPTSROOT") )
      if opts["det"] == "frozen":
        playlistDir = os.path.join( playlistTopDir, "downstream/%s"%(releaseName) )
        opts["playlist"] = os.path.join( playlistDir, "playlist_downstream1_le010z-185i_ovrly.dat" )
      else:
        print "DIRECTORY : ", playlistTopDir
        playlistDir = os.path.join( playlistTopDir, "minerva/%s"%(releaseName) )
        print playlistDir, "*************"
        if opts["beam"] == "le010z185i": #matches 1,7,9,13
          opts["playlist"] = os.path.join( playlistDir, "playlist_minerva1_le010z185i_ovrly.dat" )
        elif opts["beam"] == "le100z200i": #matches 2,11
          opts["playlist"] = os.path.join( playlistDir, "playlist_minerva2_le100z200i_ovrly.dat" )
        elif opts["beam"] == "le100z-200i": #matches 3, 12
          opts["playlist"] = os.path.join( playlistDir, "playlist_minerva3_le100z-200i_ovrly.dat" )
        elif opts["beam"] == "le250z200i": #matches 4, 8
          opts["playlist"] = os.path.join( playlistDir, "playlist_minerva4_le250z200i_ovrly.dat" )
        elif opts["beam"] == "le010z-185i": #matches 5, 5A, 10
          opts["playlist"] = os.path.join( playlistDir, "playlist_minerva5_le010z-185i_ovrly.dat" )
        elif opts["beam"] == "le010z000i": #matches 6
          opts["playlist"] = os.path.join( playlistDir, "playlist_minerva6_le010z000i_ovrly.dat" )
        elif opts["beam"] == "me000z200i": #matches ME
          opts["playlist"] = os.path.join( playlistDir, "playlist_minervame1A_me000z200i_EmptyH2O_EmptyHe_ovrly.dat" )
        elif opts["beam"] == "me000z-200i": #matches ME anti-neutrino
          opts["playlist"] = os.path.join( playlistDir, "playlist_minervame5A_me000z-200i_FilledH2O_EmptyHe_ovly.dat" )
        else:
          if opts["overlay"]:
            raise Exception("Cannot set playlist.  There is no playlist matching beam '%s'." % opts["beam"] )
    print "I am running playlist = ", opts["playlist"]
    if opts["playlist"] != "dummy":
      if "minerva13A" in opts["playlist"]:
        opts["with_cryo"] = True
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minerva13B" in opts["playlist"]:
        opts["with_cryo"] = True
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minerva13B" in opts["playlist"]:
        opts["with_cryo"] = True
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minerva13C" in opts["playlist"]:
        opts["with_cryo"] = True
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minerva13D" in opts["playlist"]:
        opts["with_cryo"] = True  #He emptying
        opts["with_empty_cryo"] = False #He emptying
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minerva13E" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True


        #this is the start of the ME playlists....
      elif "minervame1A" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minervame1B" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minervame1C" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minervame1D" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minervame1E" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minervame1F" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minervame1G" in opts["playlist"]:
	opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minervame1L" in opts["playlist"]:
	opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame1M" in opts["playlist"]:
	opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
#These are special splits of 1M
      elif "minervame1Y" in opts["playlist"]:
	opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame1Z" in opts["playlist"]:
	opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
##End special splits of 1M
      elif "minervame1N" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame1O" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame1P" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = True

      elif "minervame5A" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame3" in opts["playlist"]:
        opts["with_cryo"] = False  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = False
      elif "minervame3A" == opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] =  False #have helium in target
        opts["with_veto"] = True
        opts["with_water"] = False


      elif "minervame6A" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6B" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6C" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6D" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False 
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6E" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False 
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6F" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = False
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6G" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6H" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6I" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6J" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True
      elif "minervame6L" in opts["playlist"]:
        opts["with_cryo"] = True  #but not full of He
        opts["with_empty_cryo"] = True
        opts["with_veto"] = True
        opts["with_water"] = True

    #set n_data_files if it has not been set by the user or standard run number
    if opts["n_data_files"] < 0 :
      opts["n_data_files"] = 8
      if( opts["det"] == "frozen" ):
        opts["n_data_files"] = 4 #frozen needs fewer data files to get enough gates

    if opts["nu_flux"] != "dummy":
      if not opts["use_histos"]:
        raise Exception("You cannot specify which neutrino types to generate unless you use histograms.")
      opts["nu_flux"] = opts["nu_flux"].split(',')
      for f in opts["nu_flux"]:
        if f.lower() not in ["nue", "nuebar", "numu", "numubar", "nutau", "nutaubar"]:
          raise Exception("PID %s cannot be used for a neutrino flux.  use only numu, numubar, nue, nuebar." % f )

    if opts["use_histos"] and opts["nu_flux"] == "dummy":
      opts["nu_flux"] = ["nue","nuebar","numu","numubar"] #default is all types

    if opts["use_histos"]:
      oldNuFlux = [ f.lower() for f in opts["nu_flux"] ]
      opts["nu_flux"] = oldNuFlux

    #if the user specifies a zCenter, but not a deltaZ assume that they wanted particles at that z position exactly
    if opts["flux_zCenter"] != "dummy" and opts["flux_deltaZ"] == "dummy":
      opts["flux_deltaZ"] = 0.001

    #if no MINOS polarity is specified, try to use beam to make a default
    if opts["bfield"] == "dummy":
      if match(r"(m|l)e\d{3}z-\d{3}i", opts["beam"]): #focus mu+ for anti-nu beam
        opts["bfield"] = -1
      else:
        opts["bfield"] = 1

      #except playlist 5A, which was antinu mode beam and nu mode MINOS magnet
      if "minerva5A" in opts["playlist"]:
        opts["bfield"] = 1

    #if the user supplied a bad value for bfield, complain
    opts["bfield"] = int(opts["bfield"])
    if not ( opts["bfield"]==1 or opts["bfield"]==-1 ):
      raise Exception("MINOS B-Field '%d' is not valid." % opts["bfield"] )

    #nobody uses argoneut for the full detector.  todo: do I need to take care to raise exception to the fool who tries?
    if opts["det"] == "minerva":
      opts["use_argo"] = False

    #if you set POT, then make sure nevents = -1 (which is the default).  If a user supplies both, complain
    if opts["pot"] != "dummy" and opts["n_events"] != -1:
      raise Exception("You specified both a number of events (%d) and a number of POT (%d).  You can only specify one of these." % (opts["n_events"], opts["pot"] ) )

    #if you didn't set nevents and you are using particle cannon. do 100 events
    if opts["n_events"] == -1 and opts["do_pc"]:
      opts["n_events"] = 100

    #if an options file is specified, make sure that it exists
    if opts["opts_file"] != "dummy" and not os.path.isfile( opts["opts_file"] ):
      raise Exception("I could not find the options file you specified '%s'." % opts["opts_file"] )

    if opts["gfilter"] != "dummy" and not os.path.isfile( opts["gfilter"] ):
      raise Exception("I could not find the gfilter options file you specified '%s'." % opts["gfilter"] )

    #if reproducing metadata then add all the metadata stuff
    if opts["reproduce_meta"]:
      opts["make_meta"] = True

    if opts["osg"]:
      os.environ["OSG_ON"] = "YES"

    if opts["osg_only"]:
      os.environ["ONLY_OSG_ON"] = "YES"

    #add trailing _ to tags if missing
    if opts["intag"] != "":
      if opts["intag"][0] != "_":
        opts["intag"] = "_" + opts["intag"]
      
    if opts["outtag"] != "":
      if opts["outtag"][0] != "_":
        opts["outtag"] = "_" + opts["outtag"]

    #prepend intag and outtag with <mc_var>
    if opts["mc_var"] != "dummy":
      if not opts["do_cal"]: #genie files do not carry <mc_var> in the tag. See MCBaseStage for more on this
        opts["intag"]  = "_"+opts["mc_var"]+opts["intag"]
      opts["outtag"] = "_"+opts["mc_var"]+opts["outtag"]

    #if a genie_var is give, change the genie_v to reflect the variation
    #THIS DOESN"T WORK GIVEN THE SAM METADATA ISN"T STORED WITH THIS GENIE_V!!!
    if opts["genie_var"] != "dummy" and opts["do_genie"]:
      opts["genie_v"] = opts["genie_v"] + "-" + opts["genie_var"]
      #if running GENIE, make sure that the variation actually exists
      known_vars = getKnownGENIEVars()
      if opts["genie_var"] not in known_vars:
        sys.exit("ERROR: Cannot create GENIE with unkown genie_var='%s'.  Know variations are: %s" % (opts["genie_var"], known_vars ) )

    if opts["genie_var"] != "dummy" and not opts["do_genie"]:
      if opts["outtag"]!="":
        opts["outtag"] = "_"+opts["genie_var"]+opts["outtag"]
      else:
        opts["outtag"] = "_"+opts["genie_var"]
      known_vars = getKnownGENIEVars()
      if opts["genie_var"] not in known_vars:
        sys.exit("ERROR: Cannot create GENIE with unkown genie_var='%s'.  Know variations are: %s" % (opts["genie_var"], known_vars ) )
      print "THIS IS IT@!!#*(*(%#(&*U", opts["outtag"]
    if opts["genie_var"] != "dummy" and not opts["do_genie"] and opts["indir"]!="dummy":#rare case we have indir with a GENIE variation
      if opts["intag"]!="":
        opts["intag"]="-"+opts["genie_var"]+opts["intag"] #intag already had the "_" check
      else:
        opts["intag"]="-"+opts["genie_var"]
    

    if (opts["no_condor"] or opts["no_grid"]) and opts["ingrid"] != True:
      opts["innogrid"] = True
    if opts["innogrid"] == True:
      opts["ingrid"] = False
    else:
      opts["ingrid"] = True

    if not opts["no_condor"] and opts["reproduce_meta"]:
      sys.exit("ERROR: You must run --reproduce_meta interactively")


    #add trailing "_" to the job prefix if missing
    if opts["job_prefix"] != "" and opts["job_prefix"][-1] != "_":
      opts["job_prefix"] += "_"

    #check if user is really an OPOS member, in minervapro's k5login, and output is directed to minervapro's area
    production_operator_users = ["drut1186","pbuitrag","vito","qhuang","mazzacan"]
    if opts["production_ops"]:
      if os.getenv("USER") not in production_operator_users:
        print "You can only use the production_ops flag if you are authorized"
        print "Authorized users are:",production_operator_users
        sys.exit("ERROR: use of production_ops flag for current user not authorized")
      k5test = os.popen("ssh minervapro@minervagpvm01 'ls'").readlines()
      if len(k5test)==0:
        print "You are not authorized to use minervapro"
        print "If you are an OPOS member please contact the MINERvA offline coordinator to be added"
        sys.exit("ERROR: user not authorized to use minervapro")

    #don't let users muck up /minerva/data/users, which must be group writable for now so users can create their user space
    userSpace  = "/users/%s/" % os.getenv("USER")
    if("/pnfs/minerva" not in opts["outdir"] and not opts["no_condor"]):#are you using dcache for grid jobs?
      sys.exit("ERROR:You must write to dCache. Please choose /pnfs/minerva/persistent/users/%s or /pnfs/minerva/scratch/users/%s. If you have questions which is best for your needs email the software list. Remember the scratch space will remove your files at some point in the future."%(os.getenv("USER"),os.getenv("USER")))
                   
    if os.getenv("USER") not in ["minervadat","minervapro"] and ( userSpace not in opts["outdir"]):
      griduserSpace  = "/users/%s/" % os.getenv("GRID_USER")
      if griduserSpace not in opts["outdir"]:
        sys.exit("ERROR: You must write to a location in your user space: %s" % userSpace)

    #check that the user isn't trying to run on submission nodes
    submitHosts = ["if01.fnal.gov","gpsn01.fnal.gov"]
    host = socket.gethostname()
    if opts["no_condor"]:
      opts["make_dag"] = False;
      if host in submitHosts:
        raise Exception("You shouldn't be on submission node '%s'.  Move to a different machine ASAP." % host)

    if opts["sl6compat"]:
      opts["grid_args"]+= " --OS=SL6 --environment=LD_LIBRARY_PATH "
  ##################################
  #Apply the standard run options
  ##################################
  def applyStandardRun( self, opts ):
    """Overwrite any relevant unset options with the options for a standard run."""
    run = opts["run"]
    if run >= 1000 and run < 10000 and not opts["ignore_warnings"]:
      print """
**************************************************************************************************
**WARNING: Runs between 1000-9999 are no long standard runs.  No standard defaults will be applied.
**The new standard run format is BBGSXX:
**    BB = beam, G = fiducial geometry, S = spill type, XX = allows for multiple copies with same BBGS
**For more details see: https://cdcvs.fnal.gov/redmine/projects/minerva-sw/wiki/Monte_Carlo_Production_Run_Numbers
**************************************************************************************************
**If you really must use run numbers 1000-9999 add the --ignore_warnings flag, but remember
**  that no standard run settings will be applied.
**************************************************************************************************
"""
      sys.exit()

    if run < 10000:
      if not ( opts["multinu"] or opts["overlay"] ):
        opts["singlenu"] = True
        opts["multinu"] = False
        opts["overlay"] = False
    else:
      if not ( opts["overlay"] or opts["minos_overlay"] ) :
        if not( opts["overlay"] ):
          opts["overlay"] = False
        elif not( opts["minos_overlay"] ) :
          opts["minos_overlay"] = False

      self.applyStandardNeutrinoRun( opts )


  def applyStandardNeutrinoRun( self, opts ):
    """Overwrite any relevant unset options with the options for a standard neutrino run.  Runs range from 1000-10000."""
    run = opts["run"]
    print "applying standard neutrino run for run %d..." % run
    stdrun = MCStandardRun( run=run, det=opts["det"] )
    print "Before applying your specifications: %s" % stdrun

    if opts["beam"] == "dummy":
      opts["beam"] = stdrun.getBeam()
    if opts["geo"] == "dummy":
      opts["geo"] = stdrun.getGeom()
    if opts["n_events"] == -1 and opts["pot"] == "dummy":
      opts["pot"] = stdrun.getPOTInFile()
    #standard neutrino runs have 200 subruns
    if opts["first_subrun"] == -1:
      opts["first_subrun"] = stdrun.getFirstSubrun()
    if opts["last_subrun"] == -1:
      opts["last_subrun"] = stdrun.getLastSubrun()

    #standard neutrino runs use event overlap
    if not (opts["singlenu"] or opts["multinu"] or opts["overlay"]):
      if stdrun.getSpillType() == "singlenu":
        opts["singlenu"] = True
        opts["multinu"]  = False
        opts["overlay"]  = False
      elif stdrun.getSpillType() == "multinu":
        opts["singlenu"] = False
        opts["multinu"]  = True
        opts["overlay"]  = False
      elif stdrun.getSpillType() == "overlay":
        opts["singlenu"] = False
        opts["multinu"]  = False
        opts["overlay"]  = True
        if(post_resurrection_era() and opts["force_minos_overlay_off"]==False):#if software release is post resurrection enable minos overlay
          opts["minos_overlay"] = True
        if opts["playlist"] == "dummy":
          opts["playlist"] = stdrun.getPlaylist()

    if opts["force_minerva_overlay_off"]==True:
      opts["overlay"] = False
      opts["force_minos_overlay_off"] = True

    #    if opts["n_data_files"] < 0:
    opts["n_data_files"] = stdrun.getNDataOverlayFiles()

    info = []
    info.append( "Beam = %s" % opts["beam"] )
    info.append( "Geometry = %s" % opts["geo"] )
    if( opts["singlenu"] ):
      info.append( "singlenu spill mode" )
    elif( opts["multinu"] ):
      info.append( "multinu spill mode" )
    elif( opts["overlay"] ):
      info.append( "overlay spill mode" )
      info.append( "%d data files" % opts["n_data_files"] )
    if opts["n_events"] == -1:
      info.append( "POT per File = %.0E" % float(opts["pot"])  )
    else:
      info.append( "N events = %d" % opts["n_events"] )
    info.append( "Subrun Range = %d-%d" % ( opts["first_subrun"], opts["last_subrun"] ) )
    print "After applying your specifications: %s" % ", ".join(info)



#################################################
# printPCHelp
#################################################
def printPCHelp():
  """Print out instructions on how to specify a ParticleCannon."""
  print "="*60
  print "="*10 + "Particle Cannon Help" + "="*10
  print "Particle Parameters:"
  print " --flux <flux>  : Comma separated list of PDG codes. (required)"
  print " --vertex <name>: Name of the Vertex selection (vertex must be defined in args)"
  print " --name <name>  : Name this Particle tool. (cannot be repeated)"
  print " --momentum <p> : Momentum of the particles in GeV/c"
  print " --minp <p>     : Minimum Momentum of the particles in GeV/c"
  print " --maxp <p>     : Maximum Momentum of the particles in GeV/c"
  print " --theta <t>    : Theta of the particles in rad"
  print " --thetamin <t> : Minimum Theta of the particles in rad"
  print " --thetamax <t> : Maximum Theta of the particles in rad"
  print " --phi <p>      : Phi of the particles in rad"
  print " --phimin <p>   : Minimum Phi of the particles in rad"
  print " --phimax <p>   : Maximum Phi of the particles in rad"
  print "Defaults if the flux has a Hadron:"
  print " name=Hadrons, minp=.15, maxp=1.8, thetamin=pi/6, thetamax=pi/1.5, phimin=-pi, phimax=pi"
  print "Defaults if the flux does not have a Hadron:"
  print " name=NonHadrons, minp=.3, maxp=10, thetamin=0, thetamax=pi/4, phimin=-pi, phimax=pi"
  print "-"*30
  print "Vertex Parameters:"
  print " --name <name>  : Name of the Vertex/ParticleCannon.  (default is 'PC')"
  print " --time <t>     : Time of flux in ns.     (default is 0)"
  print " --xCenter <x>  : X-Center of flux in mm. (default is 0)"
  print " --yCenter <y>  : Y-Center of flux in mm. (default is 0)"
  print " --zCenter <z>  : Z-Center of flux in mm. (default is 5000)"
  print " --deltaZ <dz>  : Allowed deviation from Z-Center for flux in mm (default is 1000 or 0 if zCenter is specified)"
  print " --radius <r>   : Allowed radius around flux center in mm (default is 800)"
  print "Vertex Rules:"
  print "  Enclose the parameters inside quotes after the tag \"--vertex\"."
  print "  You may associate many particles with the vertex."
  print "  If no vertex is associated with the particle, then the arguments to the global flux parameters (or opts defaults) will be used."
  print "Examples:"
  print "  --particle \"--name proton --flux 2212 --minp .1 --maxp .2\" --zCenter 5500 --deltaZ 250"
  print "  --particle \"--name muon --flux 13,-13 --momentum 3\" --particle \"--name hadrons --flux 2212,211,-211 --theta 0\" --radius 200"
  print "  --particle \"--flux 13\" --particle \"--flux -13 --vertex vtxA\" --vertex \"--name vtxA --zCenter 6000\""
  print "  --particle \"--flux 2212 --vertex proton\" --particle \"--flux -13 --vertex muon\" --vertex \"--name proton --time 1200\" --vertex \"--name muon --time 1000\""
  print "="*60


#################################################
# getKnownGENIEVars
#################################################
def getKnownGENIEVars():
  known_vars = ["genie2.8","CCCOHSignal","NUEELSignal","NCDiffSignal","HNLSignal"] #TMP - special type not set with physics options
  userOptsFile = os.path.join( os.getenv("GENIE"), "config/UserPhysicsOptions.xml"  )
  for line in open(userOptsFile):
    if "param_set" in line:
      m = match( r'\s*<\s*param_set\s*name\s*=\s*"(.*)"\s*>', line )
      if m and m.group(1) != "Default":
        known_vars.append( m.group(1) )

  # make RockMuon a legit "GENIE" version to get it into metadata
  known_vars.append("RockMuon")
  known_vars.append("SpecialSignal")
  known_vars.append("SpecialSpecialSignal")
  return known_vars


#################################################
# printGENIEVarHelp
#################################################
def printGENIEVarHelp():
  """Print out instructions on how to specify a GenieVariation."""
  print "="*60
  print "="*10 + " GENIE Variation Help " + "="*10
  print "Supply a string for the GENIE Variation, <genie_var>."
  print "Output will go to <outdir>/<genie_var>/<det>/..."
  print "Input will come from <outdir>/<genie_var>/<det>/..."
  print "Input and output filenames will have <genie_var> after the genie version."
  print "-"*60
  print "GENIE Variations are created with custom UserPhysicsOptions in $GENIEROOT/minerva/UserPhysicsOptions."
  print "A param_set in GENIE/minerva/UserPhysicsOptions.xml named <genie_var> must exist for you to run the variation."
  print "*"*10 + " LIST OF KNOWN VARIATIONS " + "*"*10
  print getKnownGENIEVars()
  print "*"*45
  print "="*60


#################################################
# printMCVarHelp
#################################################
def printMCVarHelp():
  """Print out instructions on how to specify an MCVariation."""
  known_vars = []
  for f in glob.glob(os.getenv("SYSTEMTESTSROOT") + "/options/MCVariations/MCCal-NuMI_*.opts"):
    m = match( r'MCCal-NuMI_(.*)\.opts', os.path.basename(f) )
    if m:
      known_vars.append( m.group(1) )
  print "="*60
  print "="*10 + " MC Variation Help " + "="*10
  print "Supply a string for the MC Variation, <mc_var>."
  print "The output will go to <outdir>/<mc_var>/<det>/..."
  print "The input, except for GENIE, will come from <indir>/<mc_var>/<det>/....  All genie input is assumed to be central value."
  print "Input and output filenames (except genie) will have <mc_var> after the software release."
  print "If no variation is supplied, then \"central_value\" is used instead of an <mc_var>."
  print "-"*60
  print "MC Variation options files are in $SYSTEMTESTSROOT/options/MCVariations/"
  print "An options file named MCCal-NuMI_<mc_var>.opts must exist for you to run the variation."
  print "*"*10 + " LIST OF KNOWN VARIATIONS " + "*"*10
  print known_vars
  print "*"*45
  print "="*60

#################################################
# getLatterStageOptions
#################################################
def getLatterStageOptions(opts):
  """Get a copy of opts with any options changed appropriately for the
     non-first stages in a dag. Eg, the second stage must have its indir
     set to the outdir of the first stage, etc"""
  ret=opts.copy()
  if opts.has_key("outdir"):
    ret["indir"]=opts["outdir"]
    ret["indir"].replace("%s"%opts["inv"],"%s"%(os.getenv("MINERVA_RELEASE")))
  if opts.has_key("outtag"):
    ret["intag"]=opts["outtag"]

  return ret

#################################################
# addStage
#################################################
def addStage(stages, stage, opts):
  """Add a stage (specified by its class name) to the list of stages to be run. If it's not the first stage in the list, mangle its options using getLatterStageOptions()"""
  if len(stages)==0:
    stages.append(stage(opts))
  else:
    stages.append(stage(getLatterStageOptions(opts)))
   
#################################################
# validateOptions 
##################################################
def validateOptions(opts):

  if not opts["kill_switch"] == False:
    sys.exit( "ERROR: You are trying to run Production Scripts in 'Expert Mode' without doing the secret Expert voodoo that will make it work. You should be using ProductionScriptsLite instead!!" )

################################################
# 
# Definition of pre-Eroica and post for minos overlay application
#
################################################

def post_resurrection_era():
  
  swrel = os.getenv("MINERVA_RELEASE")

  v = int(swrel[1:swrel.find("r")])
  r = -1
  if(swrel.find("p")!=-1):
    r = int(swrel[swrel.find("r")+1:swrel.find("p")])
  else:
    r = int(swrel[swrel.find("r")+1:])

  if v > 10 or (v == 10 and r >= 8):
    return True
  else:
    return False

################################################
# 
# Definition of release name
#
################################################

def getReleaseName():
  
  return os.getenv("MNVPRODUCTION_VERSION")
  
#################################################
#
#This script will process minerva MC
#Execute with no arguments or --help to get instructions
#
#################################################
def main():

  if os.getenv("User_release_area").find("/afs/") != -1:
    print "Your CMT USER AREA IS IN AFS - can't do that "
    sys.exit(1)

  if len(sys.argv) < 2: #if no arguments are given, give the user help
    parserHelp = MCOptionParser()
    parserHelp.addOpts_Help()
    parserHelp.showHelp()
  #Set default environmental variables (used to be done in the requirement file)
  envvariables = MinervaProductionMap()
  envvariables.setProductionMap()
  parser = MCOptionParser()
  parser.addOpts_All()

  (opts, args) = parser.parse_args(sys.argv)
  opts = vars(opts)#turn options into a dictionary

  #does the user want help?
  if opts["help_action"]:
    parserHelp = MCOptionParser()
    parserHelp.addOpts_WhatToRun()
    parserHelp.showHelp()
  if opts["help_common"]:
    parserHelp = MCOptionParser()
    parserHelp.addOpts_Common()
    parserHelp.showHelp()
  if opts["help_jobsub"]:
    parserHelp = MCOptionParser()
    parserHelp.addOpts_JobSub()
    parserHelp.showHelp()
  if opts["help_io"]:
    parserHelp = MCOptionParser()
    parserHelp.addOpts_IO()
    parserHelp.showHelp()
  if opts["help_genie"]:
    parserHelp = MCOptionParser()
    parserHelp.addOpts_Genie()
    parserHelp.showHelp()
  if opts["help_pc"]:
    printPCHelp()
    parserHelp = MCOptionParser()
    parserHelp.addOpts_Flux()
    parserHelp.showHelp()
  if opts["help_af"]:
    parserHelp = MCOptionParser()
    parserHelp.addOpts_AF()
    parserHelp.showHelp()
  if opts["help_minos"]:
    parserHelp = MCOptionParser()
    parserHelp.addOpts_Minos()
    parserHelp.showHelp()
  if opts["help_genie_var"]:
    printGENIEVarHelp()
    sys.exit(0)
  if opts["help_mc_var"]:
    printMCVarHelp()
    sys.exit(0)


  #Warn the user that this is not the right script for analysis tools (temporary)
  if opts["ana_tool"]:
    sys.exit("NOTICE: This script is no longer used to run minerva analysis tools.  Use ProductionScripts/ana_scripts/ProcessAna.py instead.")

  #Warn user of possible risks of using SL7 for non-Minos MC and SL6 for minos MC.
  if (opts["os"] == "SL7" or opts["os"] == "sl7") and opts["do_minos"] and not opts["minos_SL7_override"]:
    sys.exit("WARNING: MINOS jobs CANNOT currently be run with SL7, and must be run using the SL6 Singularity Container once the Grid Nodes migrate to SL7 (Effective Sept. 30, 2020). Overriding this warning by passing the --minos_sl7_override option comes with the risks of running a minos stage job in SL6 though other MC stages were run in SL7. Overriding this warning acknowledges and assumes these risks.")

  #Clarify that the OS of SL6 means a Singularity container from here on out.
  if (opts["os"] == "SL6" or opts["os"] == "sl6"):
    print "NOTICE: Fermilab Grid Nodes have migrated to SL7 (Effective Sept. 30, 2020). Your jobs will be run using the latest Fermilab Singularity Image for SL6. For more information, see the following wiki about Singularity jobs: https://cdcvs.fnal.gov/redmine/projects/fife/wiki/Singularity_jobs. Ignore if running interactively."

  #overwrite any dummy variables with standard run options if applicable
  try:
    parser.applyStandardRun( opts )
  except Exception, e:#todo comma is for python 2.5
    sys.exit("Error in applying standard run:\n\t%s" % e )

  #overwrite any dummy variables an
  try:
    parser.applyDefaults( opts )
  except Exception, e:#todo comma is for python 2.5
    sys.exit("Error in applying defaults:\n\t%s" % e)

  if opts["do_cal"] and opts["do_pc"]:
    sys.exit("ERROR: You cannot run both the Cal (from GENIE) and Cal-PC (particle cannon) stages.  You must select one.")

  stages = []
  copyopts = opts
  if opts["do_genie"]:   addStage(stages, MCGenieStage,    opts)
  if opts["do_cal"]:     addStage(stages, MCCalStage,      opts)
  if opts["do_rock"]:    addStage(stages, MCCalRockStage,  opts)
  if opts["do_pc"]:      addStage(stages, MCCalPCStage,    opts)
  if opts["do_pc_ext"]:  addStage(stages, MCCalPCExtStage, opts)
  if opts["do_mc_muon"]: addStage(stages, MCMuonStage,     opts)
  if opts["do_argo"]:    addStage(stages, MCArgoStage,     opts)
  if opts["do_minos"]:   addStage(stages, MCMinosStage,    opts)
  if opts["do_reco"]:    addStage(stages, MCRecoStage,     opts)
  if opts["do_ana"]:     addStage(stages, MCAnaStage,      opts)

  if opts["do_meu"]:     addStage(stages, MCMEUStage,      opts)

  #DSTWriter is its own stage when:
  #   A) No other action is requested
  #   B) Running in a DAG with a stage that produces a POOL output
  #Otherwise DSTWriter is just part of the action you requested (if applicable)
  if opts["do_dst"]:
    if len(stages) == 0 or \
       ( opts["make_dag"] and \
         (opts["do_cal"] or opts["do_pc"] or opts["do_rock"] or opts["do_reco"]) \
         and not opts["do_ana"] ):
      #if doing cal stage but not reco, then use Cal output to make the DST
      if opts["do_cal"] and not opts["do_reco"]:
        opts["dst_fCal"] = True
        copyopts["dst_fCal"] = True
      addStage(stages, MCDSTStage, opts)

  if len(stages) == 0:
    parserHelp = MCOptionParser()
    parserHelp.addOpts_WhatToRun( )
    print "ERROR: You must tell me what to run.  Here's your help..."
    print #extra line
    parserHelp.parse_args( "--help".split() )

  if len( stages ) > 1 and opts["opts_file"] != "dummy":
    print "OPTS:",opts["opts_file"]
    sys.exit("ERROR: You can not specify an options file if you run more than one action.")

  if opts["do_meu"] and len(stages) > 1:
    sys.exit("ERROR: If you run the meu stage, you cannot run any others.")

  print "Options !!!!!! => ", opts
  ##############################
  # Stuff starts happening here
  # If testing exit now
  ##############################
  if opts["test"]:
    sys.exit()

  ########## DAG ###########
  # if making a DAG, check that first stage for each subrun has input
  # if it does not have input add it to the exclude list
  # if it does have input, create the necessary opts/script
  #     then create and submit the DAG file
  ###########################
  if opts["make_dag"]:
    #todo check that the supplied stages make sense (e.g. not genie->reco)
    submit_dag_options = ""

    submit_dag_command = "jobsub_submit_dag --group=minerva "
    if os.environ.has_key("JOBSUB_SERVER"):
      submit_dag_command += " --jobsub-server=%s " % os.getenv("JOBSUB_SERVER")
    if "minerva" in os.getenv("USER") or opts["production_ops"]:
      submit_dag_command += " --role=Production --subgroup=Production"
      ##jobsub_lite did not consider SL6 singularity. In order to have production jobs we now need to pass the tarball
      ##to the "jobsub_submit_dag" command as "-f dropbox://tarballpath/tarballname.tgz, and not to the "jobsub" command.
      ##We need to create the timestamp of the tarball name here and pass it to the "makeTarBall" function, so they are
      ##the same.
      if os.path.isdir(opts["outdir"]) :
        outdirlist = os.listdir(opts["outdir"])
        if len(outdirlist) != 0 :
          for f in outdirlist :
            if f.endswith(".tgz") :
              tarball_fullname = "%s/%s" % (opts["resilient_dir"], f)
      elif( os.path.isfile( "%s" % opts["tarball"]) ):
        print "A tarball option has been passed, keeping the tarball in DAG..."
        tarball_fullname = "%s" % opts["tarball"]
      else :
        tardt = "%s" % (datetime.now().strftime("%Y%m%d_%H%M%S"))
        tarball_name_nodatetime = "{user}_{version}".format(user=os.getenv("USER"), version=os.getenv("MINERVA_RELEASE"))
        tarball_fullname = "%s/%s-%s.tgz" % (opts["resilient_dir"], tarball_name_nodatetime, tardt)
      tarball_name = tarball_fullname.split("/")[-1]
      submit_dag_command += " -f dropbox://%s " % tarball_fullname
      #"--use-pnfs-dropbox" is use when RCDS is in a degraded state
      ###submit_dag_command += " --use-pnfs-dropbox -f dropbox://%s " % tarball_fullname
    os.environ["IFDH_CP_MAXRETRIES"] = "0"
    if "IFDH_DEBUG" in os.environ.keys():
      submit_dag_command += " -e IFDH_DEBUG "
    if "IFDH_CP_MAXRETRIES" in os.environ.keys():
      submit_dag_command += " -e IFDH_CP_MAXRETRIES "
    ###if "IFDH_VERSION" in os.environ.keys():   #<NOTE: Vito from SCD suggested to remove these
    ###  submit_dag_command += " -e IFDH_VERSION "
    ####always specify the minerva version of IFDHC
    ###if "IFDHC_VERSION" in os.environ.keys():
    ###  submit_dag_command += " -e IFDHC_VERSION "
    submit_dag_command += " -e IFDH_FORCE=gsiftp -e IFDH_TOKEN_ENABLE=0 -e IFDH_PROXY_ENABLE=1 "

    #check for variables in grid_args (this is really no pythonic code...
    if opts["grid_args"] and opts["grid_args"].find("-e")!=-1:
      tmp_string = opts["grid_args"].split()#now we look for -e and the variable after that
      for i,el in enumerate(tmp_string):
        if el == "-e":
          if "%s"%(tmp_string[i+1]) in os.environ.keys():
            submit_dag_command += " -e %s "%(tmp_string[i+1])
          else:
            sys.exit("You need to set the %s to a value before you submit"%(tmp_string[i+1])) 
    exclude_subs = []

    # If the first stage *does* have input, create the .dag-input file
    # First pick a name for the .dag-input file and create it
    dt = datetime.today( )
    timestamp = dt.strftime("%Y_%m_%d-%H_%M")
    timestamp += "_%02d" % dt.second
    dag_dir = stages[0].dag_out
    if(opts["dag_loc"] != ""):
      dag_dir = opts["dag_loc"]

    if not os.path.isdir( dag_dir ):
      os.makedirs( dag_dir )

    dag_input_file = dag_dir + "/processMC_Run"+str(opts["run"]) + "-" + timestamp + ".dag-input"
    print dag_input_file
    dag = open( dag_input_file, "w" )
    ###dag.write("#This DAG was created by the command:\n") #NOTE: <JOBSUBLITE TESTING, ORIGINAL>
    ###dag.write("#" + ' '.join(sys.argv) + "\n") #NOTE: <JOBSUBLITE TESTING, ORIGINAL>

    # Have to start the dag with a dummy <serial> section, because of limitations in jobsub_submit_dag.
    # See https://cdcvs.fnal.gov/redmine/issues/10152
    #
    # Request lower memory and time for the dummy job, in the hope
    # that it'll get scheduled quicker.  300MB and 50m is w-a-a-a-a-y
    # more than it'll actually need, but I tried less and jobs got
    # held. Someone with more patience than me can try optimizing it
    # if they want
###    dag.write("""
###<serial>
###jobsub -n --memory 300MB --expected-lifetime 50m file:///bin/true
###</serial>
###""")

    dag.write("<parallel>\n")
    # Make a dag for every run
    for i,sub in enumerate(opts["subruns"]):
      # For jobsub_submit_dag, we need a dag input file (which the
      # jobsub system turns into a condor dag) which looks something
      # like:
      #
      # <serial>
      # jobsub --some-options --some-more-options wrapper_for_stage1
      # jobsub --some-options --some-more-options wrapper_for_stage2
      # ... etc ..
      # </serial>
      #
      # So we're going to loop through the stages, and for each one
      # get the jobsub command that it would use if we were just
      # submitting that stage. Then we write those commands into the
      # dag input file and jobsub_submit_dag it. Note that the opts
      # for each stage may have been mangled by addStage() when we
      # added the stage to the list of stages.
           #Only can submit 500 jobs in a dag without overloading the system.
      if(i%500==500):
        dag.write("</parallel>") #<NOTE: JOBSUB_LITE TESTING BLOCK ORIGINAL>
###        dag.write("""
###<serial>
###jobsub -n --memory 300MB --expected-lifetime 50m file:///bin/true
###</serial>""")
        dag.close()
        
        cmd = "%s file://%s " % (submit_dag_command, dag_input_file)
        
        dagProc = subprocess.Popen( cmd.split(), shell=False )#build and submit the DAG
        dagProc.wait() #wait for command to finish
        # If the first stage *does* have input, create the .dag-input file
        # First pick a name for the .dag-input file and create it
        dt = datetime.today( )
        timestamp = dt.strftime("%Y_%m_%d-%H_%M")
        timestamp += "_%02d" % dt.second
        dag_dir = stages[0].dag_out
        if(opts["dag_loc"] != ""):
          dag_dir = opts["dag_loc"]
        if not os.path.isdir( dag_dir ):
          os.makedirs( dag_dir )

        dag_input_file = dag_dir + "/processMC_Run"+str(opts["run"]) + "-" + timestamp + ".dag-input"
        print dag_input_file
        dag = open( dag_input_file, "w" )
        ###dag.write("#This DAG was created by the command:\n")
        ###dag.write("#" + ' '.join(sys.argv) + "\n")
        
        # Have to start the dag with a dummy <serial> section, because of limitations in jobsub_submit_dag.
        # See https://cdcvs.fnal.gov/redmine/issues/10152
###        dag.write("""
###<serial>
###jobsub -n  --memory 300MB --expected-lifetime 50m file:///bin/true
###</serial>
###""")
        dag.write("<parallel>\n")
      #end if
      # If the first stage can't find its input files, skip the subrun
      stages[0].prepareSubrun( sub, opts['usecat'])
      if not stages[0].inputIsGood():
        print "Couldn't find input for subrun %d. Skipping" % sub
        continue

      dag.write("<serial>\n")
      # Now populate the .dag-input file usefully
      for index, stage in enumerate(stages):
        print "      Adding %s to DAG" % stage.__class__.__name__
        # We don't want the stage to actually submit. This turns off any checks
        stage.no_submit=True
        # Annoyingly, dag input files have to start "jobsub
        # [options...]", even though they're really jobsub_submit
        # options. We only get to this point if --use_jobsub_client
        # was passed, and so CondorJobBuilder has its jobsub set to
        # jobsub_submit. Reset it here, which is ugly, but meh

	#04/10/2015 DR: Added "-f --force=gridftp" to the jobsub command 
	#below to temporarily fix the grid map certification issue for 
	#shared account like minervapro (ServiceDesk ticket:INC000000529000)
	#which was preventing minervapro files being copied to dcache area. 
	#This needs to be the FIRST -f optionin the submit command. The 
	#"ifdh cp" comamnd will end up with a command like "ifdh cp -D 
	# --force=gridftp". 
 
        #stage.jobsub_cmd="jobsub -f --force=gridftp"
	#with the JOBSUB_CLIENT_VERSION v1_1_1 we can take "-f --force=gridftp" option off 

        stage.jobsub_cmd="jobsub "
        
        stage.prepareSubrun( sub, opts['usecat'] if index==0 else False)

        stage.prepare(tarball_name)

        # Bah, the actual thing that gets run on the grid is self.wrapper, not self.executable. Grrr.
          
        # Also, it turns out that the options that go in a dag
        # input file are not *quite* the same as if we were really
        # submitting: for example, passing --role there produces an
        # error (we have to give that option to jobsub_submit_dag
        # directly), so we use getSubmissionCommandForDAGInput() to get
        # the command
        cmd=stage.getSubmissionCommandForDAGInput(stage.wrapper)
        dag.write(cmd+"\n")
        

      dag.write("</serial>\n")
    dag.write("</parallel>\n") #<NOTE: JOBSUB_LITE TESTING ORIGINAL>
    # Have to also end the dag with a dummy <serial> section, because of limitations in jobsub_submit_dag.
    # See https://cdcvs.fnal.gov/redmine/issues/10152
###    dag.write("""
###<serial>
###jobsub -n --memory 300MB --expected-lifetime 50m file:///bin/true
###</serial>""")
    dag.close()
    

    cmd = "%s file://%s " % (submit_dag_command, dag_input_file )
    print "cmd = %s" % cmd

    dagProc = subprocess.Popen( cmd.split(), shell=False )#build and submit the DAG
    dagProc.wait() #wait for command to finish
    sys.exit()


  ######## RUN ##########
  # if not making DAG, check the input of each subrun
  # if it exists then submit
  #######################
  if len(stages) > 1:
    sys.exit("You can only supply more than one stage if you are creating a DAG")

  stage = stages[0]
  for sub in opts["subruns"]:
    if opts["exclude_subs"]!="dummy" and sub in opts["exclude_subs"]:
      print "Skipping subrun %d because it is in the exclude list..."%sub
      continue

    # only used usecat for first stage

    if stage == stages[0]:

      stage.prepareSubrun( sub, opts['usecat'])
    else:
      print " no usecat for stage ", stage.my_tag
      stage.prepareSubrun(sub)

    if opts["no_submit"] or stage.inputIsGood():#todo want to catch exceptions from inputIsGood
      try:
        stage.execute()

      except Exception, e:
        print "Error running %s:\n\t%s" % (stage.__class__.__name__, e) #todo should we exit here?
        raise
      if not opts["no_submit"] and not opts["reproduce_meta"]:
        time.sleep( opts["pause"] )
    else:
      print "Skipping subrun %d because of bad input..."%sub


if __name__=="__main__":
  main()
